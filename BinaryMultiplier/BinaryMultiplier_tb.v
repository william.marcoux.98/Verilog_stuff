`include "BinaryMultiplier.v"

module main_tb();

	wire carry_out;
	wire[15:0] result;
	
	//HalfAdder_RTL u0(sum, carry_out, a, b);
	Multiplier_8b_RTL u0(result, 8'd8, 8'd9);
	
	initial begin
		$dumpfile("Multiplier_test.vcd");
		$dumpvars(0, main_tb);
		
		
		#5 $finish;
	end

endmodule