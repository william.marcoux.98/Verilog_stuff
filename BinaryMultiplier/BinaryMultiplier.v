/**
* @file BinaryMultiplier.v
* @brief File describing binary multipliers on different abstraction levels
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date January 9th, 2018
*
* This file has been written for two reasons:
*	a) Putting in practice what I learned about Verilog
*	b) Making my portfolio bigger
*/

`include "BinarySubAdder.v"


/**
* @module Multiplier_4b_RTL
* @brief 4 bit binary multiplier with a register transfer level of abstraction.
* @output product Product of the multiplier and multiplicand inputs.
* @input multiplicand First factor of the multiplication.
* @input multiplier Second factor of the multiplication.
*/
module Multiplier_4b_RTL(product, multiplicand, multiplier);

	parameter N = 4;
	
	output[(N*2)-1:0] product;
	input[N-1:0] multiplicand;
	input[N-1:0] multiplier;

	assign {product[(N*2)-1:0]} = multiplicand * multiplier;

endmodule

/**
* @module Multiplier_8b_RTL
* @brief 8 bit binary multiplier with a register transfer level of abstraction.
* @output product Product of the multiplier and multiplicand inputs.
* @input multiplicand First factor of the multiplication.
* @input multiplier Second factor of the multiplication.
*/
module Multiplier_8b_RTL(product, multiplicand, multiplier);

	parameter N = 8;
	
	output[(N*2)-1:0] product;
	input[N-1:0] multiplicand;
	input[N-1:0] multiplier;

	assign {product[(N*2)-1:0]} = multiplicand * multiplier;

endmodule

/**
* @module Multiplier_16b_RTL
* @brief 16 bit binary multiplier with a register transfer level of abstraction.
* @output product Product of the multiplier and multiplicand inputs.
* @input multiplicand First factor of the multiplication.
* @input multiplier Second factor of the multiplication.
*/
module Multiplier_16b_RTL(product, multiplicand, multiplier);

	parameter N = 16;
	
	output[(N*2)-1:0] product;
	input[N-1:0] multiplicand;
	input[N-1:0] multiplier;

	assign {product[(N*2)-1:0]} = multiplicand * multiplier;

endmodule

/**
* @module Multiplier_32b_RTL
* @brief 32 bit binary multiplier with a register transfer level of abstraction.
* @output product Product of the multiplier and multiplicand inputs.
* @input multiplicand First factor of the multiplication.
* @input multiplier Second factor of the multiplication.
*/
module Multiplier_32b_RTL(product, multiplicand, multiplier);

	parameter N = 32;
	
	output[(N*2)-1:0] product;
	input[N-1:0] multiplicand;
	input[N-1:0] multiplier;

	assign {product[(N*2)-1:0]} = multiplicand * multiplier;

endmodule

/**
* @module Multiplier_64b_RTL
* @brief 64 bit binary multiplier with a register transfer level of abstraction.
* @output product Product of the multiplier and multiplicand inputs.
* @input multiplicand First factor of the multiplication.
* @input multiplier Second factor of the multiplication.
*/
module Multiplier_64b_RTL(product, multiplicand, multiplier);

	parameter N = 64;
	
	output[(N*2)-1:0] product;
	input[N-1:0] multiplicand;
	input[N-1:0] multiplier;

	assign {product[(N*2)-1:0]} = multiplicand * multiplier;

endmodule

/**
* @module Multiplier_4b_SDF
* @brief 4 bit binary multiplier with a gate level of abstraction.
* @output product Product of the multiplier and multiplicand inputs.
* @input multiplicand First factor of the multiplication.
* @input multiplier Second factor of the multiplication.
*/
module Multiplier_4b_SDF(product, multiplicand, multiplier);

	parameter N = 4;
	
	output[(N*2)-1:0] product;
	input[N-1:0] multiplicand;
	input[N-1:0] multiplier;
	
	generate
		genvar i;
		
		wire[((N-1)*N)-1:0] adderInputs_b;
		wire[((N-1)*N)-1:0] adderInputs_a;
		assign adderInputs_b[N-1] = 1'b0;
		//AND gates
		for(i = 0; i < (N-1)*N; i = i + 1) begin : adderInputSetter
			and(adderInputs_a[i], multiplicand[i % N], multiplier[(i/N)+1]);
			if(i < N && i > 0) begin
				and(adderInputs_b[i - 1], multiplicand[i], multiplier[0]);
			end else if(i == 0) begin
				and(product[0], multiplicand[i], multiplier[0]);
			end	
		end
		//Adders
		for(i = 1; i < N; i = i + 1) begin : nDigit
			if(i == (N - 1)) begin
				SubAdder_4b_SDF sa0(product[(N-1)*2:N-1],product[(N*2)-1], adderInputs_a[(N*i)-1:(N*i)-N], adderInputs_b[(N*i)-1:(N*i)-N], 1'b0);
			end else begin									
				SubAdder_4b_SDF sa0({adderInputs_b[(i*N)+(N-2):i*N], product[i]}, adderInputs_b[(i*N)+(N-1)], adderInputs_a[(N*i)-1:(N*i)-N], adderInputs_b[(N*i)-1:(N*i)-N], 1'b0);
			end
		end
	endgenerate
	
endmodule

/**
* @module Multiplier_8b_SDF
* @brief 8 bit binary multiplier with a gate level of abstraction.
* @output product Product of the multiplier and multiplicand inputs.
* @input multiplicand First factor of the multiplication.
* @input multiplier Second factor of the multiplication.
*/
module Multiplier_8b_SDF(product, multiplicand, multiplier);

	parameter N = 8;
	
	output[(N*2)-1:0] product;
	input[N-1:0] multiplicand;
	input[N-1:0] multiplier;
	
	generate
		genvar i;
		
		wire[((N-1)*N)-1:0] adderInputs_b;
		wire[((N-1)*N)-1:0] adderInputs_a;
		assign adderInputs_b[N-1] = 1'b0;
		//AND gates
		for(i = 0; i < (N-1)*N; i = i + 1) begin : adderInputSetter
			and(adderInputs_a[i], multiplicand[i % N], multiplier[(i/N)+1]);
			if(i < N && i > 0) begin
				and(adderInputs_b[i - 1], multiplicand[i], multiplier[0]);
			end else if(i == 0) begin
				and(product[0], multiplicand[i], multiplier[0]);
			end	
		end
		//Adders
		for(i = 1; i < N; i = i + 1) begin : nDigit
			if(i == (N - 1)) begin
				SubAdder_8b_SDF sa0(product[(N-1)*2:N-1],product[(N*2)-1], adderInputs_a[(N*i)-1:(N*i)-N], adderInputs_b[(N*i)-1:(N*i)-N], 1'b0);
			end else begin									
				SubAdder_8b_SDF sa0({adderInputs_b[(i*N)+(N-2):i*N], product[i]}, adderInputs_b[(i*N)+(N-1)], adderInputs_a[(N*i)-1:(N*i)-N], adderInputs_b[(N*i)-1:(N*i)-N], 1'b0);
			end
		end
	endgenerate
	
endmodule

/**
* @module Multiplier_16b_SDF
* @brief 16 bit binary multiplier with a gate level of abstraction.
* @output product Product of the multiplier and multiplicand inputs.
* @input multiplicand First factor of the multiplication.
* @input multiplier Second factor of the multiplication.
*/
module Multiplier_16b_SDF(product, multiplicand, multiplier);

	parameter N = 16;
	
	output[(N*2)-1:0] product;
	input[N-1:0] multiplicand;
	input[N-1:0] multiplier;
	
	generate
		genvar i;
		
		wire[((N-1)*N)-1:0] adderInputs_b;
		wire[((N-1)*N)-1:0] adderInputs_a;
		assign adderInputs_b[N-1] = 1'b0;
		//AND gates
		for(i = 0; i < (N-1)*N; i = i + 1) begin : adderInputSetter
			and(adderInputs_a[i], multiplicand[i % N], multiplier[(i/N)+1]);
			if(i < N && i > 0) begin
				and(adderInputs_b[i - 1], multiplicand[i], multiplier[0]);
			end else if(i == 0) begin
				and(product[0], multiplicand[i], multiplier[0]);
			end	
		end
		//Adders
		for(i = 1; i < N; i = i + 1) begin : nDigit
			if(i == (N - 1)) begin
				SubAdder_16b_SDF sa0(product[(N-1)*2:N-1],product[(N*2)-1], adderInputs_a[(N*i)-1:(N*i)-N], adderInputs_b[(N*i)-1:(N*i)-N], 1'b0);
			end else begin									
				SubAdder_16b_SDF sa0({adderInputs_b[(i*N)+(N-2):i*N], product[i]}, adderInputs_b[(i*N)+(N-1)], adderInputs_a[(N*i)-1:(N*i)-N], adderInputs_b[(N*i)-1:(N*i)-N], 1'b0);
			end
		end
	endgenerate
	
endmodule

/**
* @module Multiplier_32b_SDF
* @brief 32 bit binary multiplier with a gate level of abstraction.
* @output product Product of the multiplier and multiplicand inputs.
* @input multiplicand First factor of the multiplication.
* @input multiplier Second factor of the multiplication.
*/
module Multiplier_32b_SDF(product, multiplicand, multiplier);

	parameter N = 32;
	
	output[(N*2)-1:0] product;
	input[N-1:0] multiplicand;
	input[N-1:0] multiplier;
	
	generate
		genvar i;
		
		wire[((N-1)*N)-1:0] adderInputs_b;
		wire[((N-1)*N)-1:0] adderInputs_a;
		assign adderInputs_b[N-1] = 1'b0;
		//AND gates
		for(i = 0; i < (N-1)*N; i = i + 1) begin : adderInputSetter
			and(adderInputs_a[i], multiplicand[i % N], multiplier[(i/N)+1]);
			if(i < N && i > 0) begin
				and(adderInputs_b[i - 1], multiplicand[i], multiplier[0]);
			end else if(i == 0) begin
				and(product[0], multiplicand[i], multiplier[0]);
			end	
		end
		//Adders
		for(i = 1; i < N; i = i + 1) begin : nDigit
			if(i == (N - 1)) begin
				SubAdder_32b_SDF sa0(product[(N-1)*2:N-1],product[(N*2)-1], adderInputs_a[(N*i)-1:(N*i)-N], adderInputs_b[(N*i)-1:(N*i)-N], 1'b0);
			end else begin									
				SubAdder_32b_SDF sa0({adderInputs_b[(i*N)+(N-2):i*N], product[i]}, adderInputs_b[(i*N)+(N-1)], adderInputs_a[(N*i)-1:(N*i)-N], adderInputs_b[(N*i)-1:(N*i)-N], 1'b0);
			end
		end
	endgenerate
	
endmodule

/**
* @module Multiplier_64b_SDF
* @brief 64 bit binary multiplier with a gate level of abstraction.
* @output product Product of the multiplier and multiplicand inputs.
* @input multiplicand First factor of the multiplication.
* @input multiplier Second factor of the multiplication.
*/
module Multiplier_64b_SDF(product, multiplicand, multiplier);

	parameter N = 64;
	
	output[(N*2)-1:0] product;
	input[N-1:0] multiplicand;
	input[N-1:0] multiplier;
	
	generate
		genvar i;
		
		wire[((N-1)*N)-1:0] adderInputs_b;
		wire[((N-1)*N)-1:0] adderInputs_a;
		assign adderInputs_b[N-1] = 1'b0;
		//AND gates
		for(i = 0; i < (N-1)*N; i = i + 1) begin : adderInputSetter
			and(adderInputs_a[i], multiplicand[i % N], multiplier[(i/N)+1]);
			if(i < N && i > 0) begin
				and(adderInputs_b[i - 1], multiplicand[i], multiplier[0]);
			end else if(i == 0) begin
				and(product[0], multiplicand[i], multiplier[0]);
			end	
		end
		//Adders
		for(i = 1; i < N; i = i + 1) begin : nDigit
			if(i == (N - 1)) begin
				SubAdder_64b_SDF sa0(product[(N-1)*2:N-1],product[(N*2)-1], adderInputs_a[(N*i)-1:(N*i)-N], adderInputs_b[(N*i)-1:(N*i)-N], 1'b0);
			end else begin									
				SubAdder_64b_SDF sa0({adderInputs_b[(i*N)+(N-2):i*N], product[i]}, adderInputs_b[(i*N)+(N-1)], adderInputs_a[(N*i)-1:(N*i)-N], adderInputs_b[(N*i)-1:(N*i)-N], 1'b0);
			end
		end
	endgenerate
	
endmodule

