`include "SR_Latch.v"

module SR_FlipFlop(Q, Qn, En, S, R);
	
	output Q, Qn;
	input En, S, R;
	
	wire[1:0] nets;
	
	and(nets[0], En, R),
	   (nets[1], En, S);
	   
	SR_Latch srl0(Q, Qn, nets[1], nets[0]);

endmodule

module SR_FlipFlop_RTL(Q, Qn, En, S, R);
	
	output Q, Qn;
	input En, S, R;
	
	reg[1:0] out;
	
	always @ (S or R or En)
		if(En) begin
			out[0] <= S;
			out[1] <= R;
		end
	
	assign Q = out[0];
	assign Qn = out[1];

endmodule

module SR_FlipFlop_SDF(Q, Qn, En, S, R);
	
	output Q, Qn;
	input En, S, R;
	
	wire[1:0] nets;
	
	and(nets[0], En, R),
	   (nets[1], En, S);
	   
	SR_Latch sr_l0(Q, Qn, nets[1], nets[0]);	
	
endmodule

