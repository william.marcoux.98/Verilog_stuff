`include "SR_FlipFlop.v"

module main_tb();

	wire Q, Qn;
	reg set, reset, enable;
	
	//SR_Latch m0(Q, Qn, set, reset);
	SR_FlipFlop m0(Q, Qn, enable, set, reset);
	
	initial begin
		$dumpfile("FlipFlops_test.vcd");
		$dumpvars(0, main_tb);
		set = 0; reset = 0; enable = 0;
		#5 reset = 1;
		#5 reset = 0; set = 1;
		#5 reset = 0; set = 0;
		#5 reset = 1; set = 0;
		#5 reset = 0; set = 0; 
		#5 enable = 1;
		#5 reset = 1;
		#5 reset = 0; set = 1;
		#5 reset = 0; set = 0;
		#5 reset = 1; set = 0;
		#5 reset = 0; set = 0; 
		#5 $finish;
	end

endmodule