`include "SR_FlipFlop.v"

module D_FlipFlop(Q, Qn, En, D);

	output Q, Qn;
	input En, D;
	
	wire Dn;
	
	not(Dn, D);
	
	SR_FlipFlop srff0(Q, Qn, En, D, Dn);
	
endmodule

module D_FlipFlop_RTL(Q, Qn, En, D);

	output Q, Qn;
	input En, D;
	
	reg[1:0] out;
	
	always @ (D or En)
		if(En) begin
			out[0] <= D;
			out[1] <= ~D;
		end
	
	assign Q = out[0];
	assign Qn = out[1];
	
endmodule

module D_FlipFlop_SDF(Q, Qn, En, D);

	output Q, Qn;
	input En, D;
	
	wire Dn;
	
	not(Dn, D);

	SR_FlipFlop srff0(Q, Qn, En, D, Dn);
	
endmodule