`include "SR_Latch.v"

module JK_FlipFlop(Q, Qn, En, K, J);

	output Q, Qn;
	input En, K, J;
	
	wire R, S;
	
	and(R, Q, K, En),
	   (S, Qn, J, En);
	
	SR_Latch srl0(Q, Qn, S, R);

endmodule

module JK_FlipFlop_RTL(Q, Qn, En, K, J);

	output Q, Qn;
	input En, K, J;
	
	reg Qreg;
	
	initial begin
		Qreg  = 1'b1;
	end
	
	always @(posedge En) begin
		case({J,K})
			2'b0_0 : Qreg <= Q   ;
			2'b0_1 : Qreg <= 1'b0;
			2'b1_0 : Qreg <= 1'b1;
			2'b1_1 : Qreg <= ~Q  ;
		endcase
  end
  
  assign Q = Qreg;
  assign Qn = ~Qreg;

endmodule

module JK_FlipFlop_SDF(Q, Qn, En, K, J);

	output Q, Qn;
	input En, K, J;
	
	wire R, S;
	
	and(R, Q, K, En),
	   (S, Qn, J, En);
	
	SR_Latch srl0(Q, Qn, S, R);

endmodule