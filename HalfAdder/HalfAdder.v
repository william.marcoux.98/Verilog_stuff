/**
* @file HalfAdder.v
* @brief File describing binary half adders on different abstraction levels
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date January 4th, 2018
*
* This file has been written for two reasons:
*	a) Putting in practice what I learned about Verilog
*	b) Making my portfolio bigger
*/


/**
* @module HalfAdder_RTL
* @brief Binary half adder with a register transfer level of abstraction.
* @output sum Sum bit of the som of the two input bits.
* @output carry_out Carry bit of the sum of the two input bits.
* @input a First addend.
* @input b Second addend.
*/
module HalfAdder_RTL(
output sum, 
output carry_out,
input a,
input b);

	assign carry_out = a & b;
	assign sum = a ^ b;
	
endmodule

/**
* @module HalfAdder_SDF
* @brief Binary half adder with a gate level of abstraction.
* @output sum Sum bit of the som of the two input bits.
* @output carry_out Carry bit of the sum of the two input bits.
* @input a First addend.
* @input b Second addend.
*/
module HalfAdder_SDF(
output sum, 
output carry_out,
input a,
input b);
	
	xor(sum, a, b);
	and(carry_out, a, b);
	
endmodule

/**
* @module HalfAdder_TTab
* @brief Binary half adder using truth tables in primitives.
* @output sum Sum bit of the som of the two input bits.
* @output carry_out Carry bit of the sum of the two input bits.
* @input a First addend.
* @input b Second addend.
*/
module HalfAdder_TTab(
output sum, 
output carry_out,
input a,
input b);

	HalfAdder_sum(sum, a, b);
	HalfAdder_carry_out(carry_out, a, b);

endmodule

/**
* @primitive HalfAdder_sum
* @brief Primitive expressing the truth table for the sum output of the half adder
* @output sum Sum bit of the som of the two input bits.
* @input a First addend.
* @input b Second addend.
*/
primitive HalfAdder_sum(
output sum,
input a,
input b);

	table
	//  a b  sum
		0 0 : 0;
		0 1 : 1;
		1 0 : 1;
		1 1 : 0;
	endtable

endprimitive


/**
* @primitive HalfAdder_carry_out
* @brief Primitive expressing the truth table for the carry_out output of the half adder.
* @output carry_out Carry bit of the sum of the two input bits.
* @input a First addend.
* @input b Second addend.
*/
primitive HalfAdder_carry_out(
output carry_out,
input a,
input b);

	table
	//  a b  sum
		0 ? : 0;
		? 0 : 0;
		1 1 : 1;
	endtable

endprimitive


/**
* @module HalfAdder
* @brief Binary half adder with a register transfer level of abstraction. It's the default option.
* @output sum Sum bit of the som of the two input bits.
* @output carry_out Carry bit of the sum of the two input bits.
* @input a First addend.
* @input b Second addend.
*/
module HalfAdder(
output sum, 
output carry_out,
input a,
input b);

	assign carry_out = a & b;
	assign sum = a ^ b;
	
endmodule