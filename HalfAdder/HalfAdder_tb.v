`include "HalfAdder.v"

module main_tb();

	reg a, b;
	wire carry_out, sum;
	
	//HalfAdder_RTL u0(sum, carry_out, a, b);
	//HalfAdder_SDF u0(sum, carry_out, a, b);
	HalfAdder_TTab u0(sum, carry_out, a, b);
	
	initial begin
		$dumpfile("HalfAdder_test.vcd");
		$dumpvars(0, main_tb);
		
		a = 0;
		b = 0;
		#5 a = 1;
		#5 b = 1;
		#5 a = 0;
		#5 $finish;
	end

endmodule