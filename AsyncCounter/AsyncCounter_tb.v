`include "AsyncCounter.v"

module main_tb();

	reg clk, reset;
	wire[3:0] result;
	
	AsyncCounter_4b_SDF u0(result, clk, reset);
	initial begin
		$dumpfile("AsyncCounter_test.vcd");
		$dumpvars(0, main_tb);
		clk = 1'b0; // Set clk to 0
		reset = 1'b1;
		#25 reset = 1'b0;
		#5 reset = 1'b0;
		#50 $finish;
	end
	   
    always
		#2 clk = ~clk;
	

endmodule