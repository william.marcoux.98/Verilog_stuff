module SR_Latch(Q, Qneg, S, R);

	output Q, Qneg;
	input R, S;
	
	nor(Q, R, Qneg);
	nor(Qneg, S, Q);
	
endmodule

module SR_Latch_RTL(Q, Qneg, S, R);
	
	output Q, Qneg;
	input R, S;
	
	wire Q_int, Qneg_int;
	
	assign Q_int = ~(S & Qneg_int);
	assign Qneg_int = ~(R & Q_int);
	assign Q = Q_int;
	assign Qneg = Qneg_int;
	
endmodule

module SR_Latch_SDF(Q, Qneg, S, R);

	output Q, Qneg;
	input R, S;
	
	nor(Q, R, Qneg);
	nor(Qneg, S, Q);
	
endmodule

module SR_Latch_NOR_SDF(Q, Qneg, S, R);

	output Q, Qneg;
	input R, S;
	
	nor(Q, R, Qneg);
	nor(Qneg, S, Q);
	
endmodule

module SR_Latch_NAND_SDF(Q, Qneg, S, R);

	output Q, Qneg;
	input R, S;
	
	nand(Q, S, Qneg);
	nand(Qneg, R, Q);
	
endmodule