`include "SR_Latch.v"

module JK_FlipFlop(q, qn, en, j, k, cl);
	
	output q, qn;
	input en, j, k, cl;
	
	wire j1,k1,q1,qn1,j2,k2,q2,qn2,en2;
	
	not n(en2, en);

	nand(j1, j, en, qn2, cl),
		(k1, k, en, q2),
		(q1, j1, qn1),
		(qn1, k1, q1, cl),
		(j2, q1, en2),
		(k2, qn1, en2),
		(q2, j2, qn2),
		(qn2, k2, q2, cl);

	assign q = q2;

	assign qn = qn2;

endmodule

module JK_FlipFlop_RTL(q, qn, en, j, k, cl);
	
	output reg q, qn;
	input en, j, k, cl;
	
	initial begin 
		q = 1'b0;
		qn = 1'b1; 
	end
	
	always @(posedge cl) begin
		q = 1'b0;
		qn = 1'b1;
	end
	
	always @(posedge en)
		begin
			case({j,k})
				{1'b0,1'b0}:begin q = q; qn = qn; end
				{1'b0,1'b1}: begin q = 1'b0; qn = 1'b1; end
				{1'b1,1'b0}:begin q = 1'b1; qn = 1'b0; end
				{1'b1,1'b1}: begin q = ~q; qn = ~qn; end
			endcase
		end

endmodule

module JK_FlipFlop_SDF(q, qn, en, j, k, cl);
	
	output q, qn;
	input en, j, k, cl;
	
	wire j1,k1,q1,qn1,j2,k2,q2,qn2,en2;
	
	not n(en2, en);

	nand(j1, j, en, qn2, cl),
		(k1, k, en, q2),
		(q1, j1, qn1),
		(qn1, k1, q1, cl),
		(j2, q1, en2),
		(k2, qn1, en2),
		(q2, j2, qn2),
		(qn2, k2, q2, cl);

	assign q = q2;

	assign qn = qn2;

endmodule