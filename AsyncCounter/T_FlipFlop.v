`include "JK_FlipFlop.v"

module T_FlipFlop(q, qn, en, t, reset);
	
	output q, qn;
	input en, t, reset;
	
	JK_FlipFlop jkff0(q, qn, en, t, t, reset);

endmodule

module T_FlipFlop_RTL(q, qn, en, t, reset);
	
	output reg q, qn;
	input en, t, reset;
	
	initial begin
		q <= 1'b0;
		qn <= 1'b1;
	end
	
	always @(posedge en, negedge reset) begin
		if (!reset) begin 
			q <= 1'b0;
			qn <= 1'b1;
		end else begin
			case (t)
				1'b0:begin q <= q; qn <= qn; end
				1'b1:begin q <= ~q; qn <= ~qn; end
			endcase
		end
	end

endmodule

module T_FlipFlop_SDF(q, qn, en, t, reset);
	
	output q, qn;
	input en, t, reset;
	
	JK_FlipFlop_SDF jkff0(q, qn, en, t, t, reset);

endmodule