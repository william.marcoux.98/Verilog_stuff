`include "T_FlipFlop.v"

module AsyncCounter_4b_SDF(count, clk, reset);
	
	parameter N = 4;
	
	output[N-1:0] count;
	input clk, reset;
	
	wire[N-1:0] countn;
	
	generate
		genvar i;
		for(i = 0; i < N; i = i + 1) begin
			if(i == 0) begin
				T_FlipFlop tff0(count[i], countn[i], clk, 1'b1, reset);
			end else begin
				T_FlipFlop tff0(count[i], countn[i], count[i-1], 1'b1, reset);
			end
		end		
	endgenerate

endmodule

module AsyncCounter_8b_SDF(count, clk, reset);
	
	parameter N = 8;
	
	output[N-1:0] count;
	input clk, reset;
	
	wire[N-1:0] countn;
	
	generate
		genvar i;
		for(i = 0; i < N; i = i + 1) begin
			if(i == 0) begin
				T_FlipFlop tff0(count[i], countn[i], clk, 1'b1, reset);
			end else begin
				T_FlipFlop tff0(count[i], countn[i], count[i-1], 1'b1, reset);
			end
		end		
	endgenerate

endmodule

module AsyncCounter_16b_SDF(count, clk, reset);
	
	parameter N = 16;
	
	output[N-1:0] count;
	input clk, reset;
	
	wire[N-1:0] countn;
	
	generate
		genvar i;
		for(i = 0; i < N; i = i + 1) begin
			if(i == 0) begin
				T_FlipFlop tff0(count[i], countn[i], clk, 1'b1, reset);
			end else begin
				T_FlipFlop tff0(count[i], countn[i], count[i-1], 1'b1, reset);
			end
		end		
	endgenerate

endmodule

module AsyncCounter_32b_SDF(count, clk, reset);
	
	parameter N = 32;
	
	output[N-1:0] count;
	input clk, reset;
	
	wire[N-1:0] countn;
	
	generate
		genvar i;
		for(i = 0; i < N; i = i + 1) begin
			if(i == 0) begin
				T_FlipFlop tff0(count[i], countn[i], clk, 1'b1, reset);
			end else begin
				T_FlipFlop tff0(count[i], countn[i], count[i-1], 1'b1, reset);
			end
		end		
	endgenerate

endmodule

module AsyncCounter_4b_RTL(count, clk, reset);
	
	parameter N = 4;
	
	output[N-1:0] count;
	input clk, reset;
	
	reg[N-1:0] count_reg;
	
	initial begin
		count_reg = 0;
	end
	
	always @(posedge reset) begin
		count_reg = 0;
	end
	
	always @(negedge clk) begin
		count_reg = (count_reg + 1) % (2**N);
	end
	
	assign count = count_reg;

endmodule

module AsyncCounter_8b_RTL(count, clk, reset);
	
	parameter N = 8;
	
	output[N-1:0] count;
	input clk, reset;
	
	reg[N-1:0] count_reg;
	
	initial begin
		count_reg = 0;
	end
	
	always @(posedge reset) begin
		count_reg = 0;
	end
	
	always @(negedge clk) begin
		count_reg = (count_reg + 1) % (2**N);
	end
	
	assign count = count_reg;

endmodule

module AsyncCounter_16b_RTL(count, clk, reset);
	
	parameter N = 16;
	
	output[N-1:0] count;
	input clk, reset;
	
	reg[N-1:0] count_reg;
	
	initial begin
		count_reg = 0;
	end
	
	always @(posedge reset) begin
		count_reg = 0;
	end
	
	always @(negedge clk) begin
		count_reg = (count_reg + 1) % (2**N);
	end
	
	assign count = count_reg;

endmodule

module AsyncCounter_32b_RTL(count, clk, reset);
	
	parameter N = 32;
	
	output[N-1:0] count;
	input clk, reset;
	
	reg[N-1:0] count_reg;
	
	initial begin
		count_reg = 0;
	end
	
	always @(posedge reset) begin
		count_reg = 0;
	end
	
	always @(negedge clk) begin
		count_reg = (count_reg + 1) % (2**N);
	end
	
	assign count = count_reg;

endmodule