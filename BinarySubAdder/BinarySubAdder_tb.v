`include "BinarySubAdder.v"

module main_tb();

	wire clk, reset;
	wire[3:0] result;
	
	//HalfAdder_RTL u0(sum, carry_out, a, b);
	SubAdder_4b_SDF u0(result, carry_out, 4'd3, 4'd2, 1'b0);
	
	   initial begin
			$dumpfile("SubAdder_test.vcd");
			$dumpvars(0, main_tb);
			clk = 1'b0; // Set clk to 0
		
			#50 $finish;
              
	   end
	   
       always
              #5 clk = ~clk;
	initial begin
		
	end

endmodule