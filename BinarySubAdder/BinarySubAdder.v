/**
* @file BinarySubAdder.v
* @brief File describing binary adders/subtractors on different abstraction levels
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date January 6th, 2018
*
* This file has been written for two reasons:
*	a) Putting in practice what I learned about Verilog
*	b) Making my portfolio bigger
*/

`include "FullAdder.v"

/**
* @module SubAdder_4b_RTL
* @brief 4 bit binary adder/subtractor with a register transfer level of abstraction.
* @output result Result of the wanted operation between num1 and num2 inputs.
* @output carry_out Carry bit of the wanted operation between num1 and num2 inputs.
* @input num1 First operation input.
* @input b Second operation input.
* @input m Operation selector. 1 for subtraction, 0 for addition.
*/
module SubAdder_4b_RTL(
output[3:0] result,
output carry_out,
input[3:0] num1,
input[3:0] num2,
input m);

	parameter N = 4;

	assign {carry_out, result[N-1:0]} = (m) ? num1 - num2 : num1 + num2;

endmodule

/**
* @module SubAdder_8b_RTL
* @brief 8 bit binary adder/subtractor with a register transfer level of abstraction.
* @output result Result of the wanted operation between num1 and num2 inputs.
* @output carry_out Carry bit of the wanted operation between num1 and num2 inputs.
* @input num1 First operation input.
* @input b Second operation input.
* @input m Operation selector. 1 for subtraction, 0 for addition.
*/
module SubAdder_8b_RTL(
output[7:0] result,
output carry_out,
input[7:0] num1,
input[7:0] num2,
input m);

	parameter N = 8;

	assign {carry_out, result[N-1:0]} = (m) ? num1 - num2 : num1 + num2;

endmodule

/**
* @module SubAdder_16b_RTL
* @brief 16 bit binary adder/subtractor with a register transfer level of abstraction.
* @output result Result of the wanted operation between num1 and num2 inputs.
* @output carry_out Carry bit of the wanted operation between num1 and num2 inputs.
* @input num1 First operation input.
* @input b Second operation input.
* @input m Operation selector. 1 for subtraction, 0 for addition.
*/
module SubAdder_16b_RTL(
output[15:0] result,
output carry_out,
input[15:0] num1,
input[15:0] num2,
input m);

	parameter N = 16;

	assign {carry_out, result[N-1:0]} = (m) ? num1 - num2 : num1 + num2;

endmodule

/**
* @module SubAdder_32b_RTL
* @brief 32 bit binary adder/subtractor with a register transfer level of abstraction.
* @output result Result of the wanted operation between num1 and num2 inputs.
* @output carry_out Carry bit of the wanted operation between num1 and num2 inputs.
* @input num1 First operation input.
* @input b Second operation input.
* @input m Operation selector. 1 for subtraction, 0 for addition.
*/
module SubAdder_32b_RTL(
output[31:0] result,
output carry_out,
input[31:0] num1,
input[31:0] num2,
input m);

	parameter N = 32;

	assign {carry_out, result[N-1:0]} = (m) ? num1 - num2 : num1 + num2;

endmodule

/**
* @module SubAdder_64b_RTL
* @brief 64 bit binary adder/subtractor with a register transfer level of abstraction.
* @output result Result of the wanted operation between num1 and num2 inputs.
* @output carry_out Carry bit of the wanted operation between num1 and num2 inputs.
* @input num1 First operation input.
* @input b Second operation input.
* @input m Operation selector. 1 for subtraction, 0 for addition.
*/
module SubAdder_64b_RTL(
output[63:0] result,
output carry_out,
input[63:0] num1,
input[63:0] num2,
input m);

	parameter N = 64;

	assign {carry_out, result[N-1:0]} = (m) ? num1 - num2 : num1 + num2;

endmodule

/**
* @module SubAdder_4b_SDF
* @brief 4 bit binary adder/subtractor with a gate level of abstraction.
* @output result Result of the wanted operation between num1 and num2 inputs.
* @output carry_out Carry bit of the wanted operation between num1 and num2 inputs.
* @input num1 First operation input.
* @input b Second operation input.
* @input m Operation selector. 1 for subtraction, 0 for addition.
*/
module SubAdder_4b_SDF(
output[3:0] result,
output carry_out,
input[3:0] num1,
input[3:0] num2,
input m);

	parameter N = 4;

	wire[N+1:0] carry_nets;
	
	assign carry_nets[0] = m;
	assign carry_out = carry_nets[N];
	
	genvar i;
	
	generate
		for(i = 0; i < N; i = i + 1) begin : addbit
			wire g0_out;
			xor g0(g0_out, num2[i], m);
			FullAdder_SDF fullAdder0(result[i], carry_nets[i + 1], carry_nets[i], num1[i], g0_out);	
		end
	endgenerate

endmodule

/**
* @module SubAdder_8b_SDF
* @brief 8 bit binary adder/subtractor with a gate level of abstraction.
* @output result Result of the wanted operation between num1 and num2 inputs.
* @output carry_out Carry bit of the wanted operation between num1 and num2 inputs.
* @input num1 First operation input.
* @input b Second operation input.
* @input m Operation selector. 1 for subtraction, 0 for addition.
*/
module SubAdder_8b_SDF(
output[7:0] result,
output carry_out,
input[7:0] num1,
input[7:0] num2,
input m);

	parameter N = 8;

	wire[N+1:0] carry_nets;
	
	assign carry_nets[0] = m;
	assign carry_out = carry_nets[N];
	
	genvar i;
	
	generate
		for(i = 0; i < N; i = i + 1) begin : addbit
			wire g0_out;
			xor g0(g0_out, num2[i], m);
			FullAdder_SDF fullAdder0(result[i], carry_nets[i + 1], carry_nets[i], num1[i], g0_out);	
		end
	endgenerate


endmodule

/**
* @module SubAdder_16b_SDF
* @brief 16 bit binary adder/subtractor with a gate level of abstraction.
* @output result Result of the wanted operation between num1 and num2 inputs.
* @output carry_out Carry bit of the wanted operation between num1 and num2 inputs.
* @input num1 First operation input.
* @input b Second operation input.
* @input m Operation selector. 1 for subtraction, 0 for addition.
*/
module SubAdder_16b_SDF(
output[15:0] result,
output carry_out,
input[15:0] num1,
input[15:0] num2,
input m);

	parameter N = 16;

	wire[N+1:0] carry_nets;
	
	assign carry_nets[0] = m;
	assign carry_out = carry_nets[N];
	
	genvar i;
	
	generate
		for(i = 0; i < N; i = i + 1) begin : addbit
			wire g0_out;
			xor g0(g0_out, num2[i], m);
			FullAdder_SDF fullAdder0(result[i], carry_nets[i + 1], carry_nets[i], num1[i], g0_out);	
		end
	endgenerate
	
endmodule

/**
* @module SubAdder_32b_SDF
* @brief 32 bit binary adder/subtractor with a gate level of abstraction.
* @output result Result of the wanted operation between num1 and num2 inputs.
* @output carry_out Carry bit of the wanted operation between num1 and num2 inputs.
* @input num1 First operation input.
* @input b Second operation input.
* @input m Operation selector. 1 for subtraction, 0 for addition.
*/
module SubAdder_32b_SDF(
output[31:0] result,
output carry_out,
input[31:0] num1,
input[31:0] num2,
input m);

	parameter N = 32;

	wire[N+1:0] carry_nets;
	
	assign carry_nets[0] = m;
	assign carry_out = carry_nets[N];
	
	genvar i;
	
	generate
		for(i = 0; i < N; i = i + 1) begin : addbit
			wire g0_out;
			xor g0(g0_out, num2[i], m);
			FullAdder_SDF fullAdder0(result[i], carry_nets[i + 1], carry_nets[i], num1[i], g0_out);	
		end
	endgenerate

endmodule

/**
* @module SubAdder_64b_SDF
* @brief  64 bit binary adder/subtractor with a gate level of abstraction.
* @output result Result of the wanted operation between num1 and num2 inputs.
* @output carry_out Carry bit of the wanted operation between num1 and num2 inputs.
* @input num1 First operation input.
* @input b Second operation input.
* @input m Operation selector. 1 for subtraction, 0 for addition.
*/
module SubAdder_64b_SDF(
output[63:0] result,
output carry_out,
input[63:0] num1,
input[63:0] num2,
input m);

	parameter N = 64;

	wire[N+1:0] carry_nets;
	
	assign carry_nets[0] = m;
	assign carry_out = carry_nets[N];
	
	genvar i;
	
	generate
		for(i = 0; i < N; i = i + 1) begin : addbit
			wire g0_out;
			xor g0(g0_out, num2[i], m);
			FullAdder_SDF fullAdder0(result[i], carry_nets[i + 1], carry_nets[i], num1[i], g0_out);	
		end
	endgenerate

endmodule
