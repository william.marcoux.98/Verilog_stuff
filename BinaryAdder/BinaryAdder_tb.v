`include "BinaryAdder.v"

module main_tb();
	
	reg[7:0] num1;
	reg[7:0] num2;
	wire[15:0] sum;
	wire carry_out;
	
	BinaryAdder_16b_SDF u0(carry_out, sum, 16'd80, 16'd256);
	
	initial begin
		$dumpfile("BinaryAdder_test.vcd");
		$dumpvars(0, main_tb);
		num1 = 128;
		num2 = 12;
		#5 $finish;
	end

endmodule