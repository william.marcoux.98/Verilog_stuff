/**
* @file BinaryAdder.v
* @brief File describing various demultiplexers.
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date January 13th, 2018
*
* This file has been written for two reasons:
*	a) Putting in practice what I learned about Verilog
*	b) Making my portfolio bigger
*/

/*
* @module Demux_1_2_RTL
* @brief Demultiplexer 1:2, with register transfer level of abstraction.
* @output out Array of outputs.
* @input in Single input.
* @input sel Selector of output(in the form of an array of bits).
*/
module Demux_1_2_RTL(out, in, sel);

	parameter N = 2;
	
	output[N-1:0] out;
	input in;
	input sel; //TODO: Find length according to N.
	
	generate
		genvar i;
		for(i = 0; i < (N - 1); i = i + 1) begin
			assign out[i] = (in & (sel == i));
		end
	endgenerate

endmodule

/*
* @module Demux_1_4_RTL
* @brief Demultiplexer 1:4, with register transfer level of abstraction.
* @output out Array of outputs.
* @input in Single input.
* @input sel Selector of output(in the form of an array of bits).
*/
module Demux_1_4_RTL(out, in, sel);

	parameter N = 4;
	
	output[N-1:0] out;
	input in;
	input[1:0] sel; //TODO: Find length according to N.
	
	generate
		genvar i;
		for(i = 0; i < (N - 1); i = i + 1) begin
			assign out[i] = (in & (sel == i));
		end
	endgenerate
	
endmodule

/*
* @module Demux_1_8_RTL
* @brief Demultiplexer 1:8, with register transfer level of abstraction.
* @output out Array of outputs.
* @input in Single input.
* @input sel Selector of output(in the form of an array of bits).
*/
module Demux_1_8_RTL(out, in, sel);

	parameter N = 8;
	
	output[N-1:0] out;
	input in;
	input[2:0] sel; //TODO: Find length according to N.
	
	generate
		genvar i;
		for(i = 0; i < (N - 1); i = i + 1) begin
			assign out[i] = (in & (sel == i));
		end
	endgenerate
	
endmodule

/*
* @module Demux_1_16_RTL
* @brief Demultiplexer 1:16, with register transfer level of abstraction.
* @output out Array of outputs.
* @input in Single input.
* @input sel Selector of output(in the form of an array of bits).
*/
module Demux_1_16_RTL(out, in, sel);

	parameter N = 16;
	
	output[N-1:0] out;
	input in;
	input[3:0] sel; //TODO: Find length according to N.
	
	generate
		genvar i;
		for(i = 0; i < (N - 1); i = i + 1) begin
			assign out[i] = (in & (sel == i));
		end
	endgenerate
	
endmodule

/*
* @module Demux_1_32_RTL
* @brief Demultiplexer 1:32, with register transfer level of abstraction.
* @output out Array of outputs.
* @input in Single input.
* @input sel Selector of output(in the form of an array of bits).
*/
module Demux_1_32_RTL(out, in, sel);

	parameter N = 32;
	
	output[N-1:0] out;
	input in;
	input[4:0] sel; //TODO: Find length according to N.
	
	generate
		genvar i;
		for(i = 0; i < (N - 1); i = i + 1) begin
			assign out[i] = (in & (sel == i));
		end
	endgenerate
	
endmodule

/*
* @module Demux_1_64_RTL
* @brief Demultiplexer 1:64, with register transfer level of abstraction.
* @output out Array of outputs.
* @input in Single input.
* @input sel Selector of output(in the form of an array of bits).
*/
module Demux_1_64_RTL(out, in, sel);

	parameter N = 64;
	
	output[N-1:0] out;
	input in;
	input[6:0] sel; //TODO: Find length according to N.
	
	generate
		genvar i;
		for(i = 0; i < (N - 1); i = i + 1) begin
			assign out[i] = (in & (sel == i));
		end
	endgenerate
	
endmodule

/*
* @module Demux_1_2_SDF
* @brief Demultiplexer 1:2, with a gate level of abstraction.
* @output out Array of outputs.
* @input in Single input.
* @input sel Selector of output(in the form of an array of bits).
*/
module Demux_1_2_SDF(out, in, sel);

	parameter N = 2;
	
	output[N-1:0] out;
	input in;
	input sel; //TODO: Find length according to N.
	
	wire sel_neg;
	
	not(sel_neg, sel);
	
	and(out[0], sel_neg, in);
	and(out[1], sel, in);
	
endmodule

/*
* @module Demux_1_4_SDF
* @brief Demultiplexer 1:4, with a gate level of abstraction.
* @output out Array of outputs.
* @input in Single input.
* @input sel Selector of output(in the form of an array of bits).
*/
module Demux_1_4_SDF(out, in, sel);

	parameter N = 4;
	
	output[N-1:0] out;
	input in;
	input[1:0] sel; //TODO: Find length according to N.
	
	wire[1:0] sel_neg;
	
	not(sel_neg[0], sel[0]),
	   (sel_neg[1], sel[1]);
	
	and(out[0], sel_neg[1], sel_neg[0], in),
	   (out[1], sel_neg[1], sel[0], in),
	   (out[2], sel[1], sel_neg[0], in),
	   (out[3], sel[1], sel[0], in);
	
endmodule

/*
* @module Demux_1_8_SDF
* @brief Demultiplexer 1:8, with a gate level of abstraction.
* @output out Array of outputs.
* @input in Single input.
* @input sel Selector of output(in the form of an array of bits).
*/
module Demux_1_8_SDF(out, in, sel);

	parameter N = 8;
	
	output[N-1:0] out;
	input in;
	input[2:0] sel; //TODO: Find length according to N.
	
	wire[2:0] sel_neg;
	
	not(sel_neg[0], sel[0]),
	   (sel_neg[1], sel[1]),
	   (sel_neg[2], sel[2]);
	
	and(out[0], sel_neg[2], sel_neg[1], sel_neg[0], in),
	   (out[1], sel_neg[2], sel_neg[1], sel[0], in),
	   (out[2], sel_neg[2], sel[1], sel_neg[0], in),
	   (out[3], sel_neg[2], sel[1], sel[0], in),
	   (out[4], sel[2], sel_neg[1], sel_neg[0], in),
	   (out[5], sel[2], sel_neg[1], sel[0], in),
	   (out[6], sel[2], sel[1], sel_neg[0], in),
	   (out[7], sel[2], sel[1], sel[0], in);
	
endmodule

/*
* @module Demux_1_16_SDF
* @brief Demultiplexer 1:16, with a gate level of abstraction.
* @output out Array of outputs.
* @input in Single input.
* @input sel Selector of output(in the form of an array of bits).
*/
module Demux_1_16_SDF(out, in, sel);

	parameter N = 16;
	
	output[N-1:0] out;
	input in;
	input[3:0] sel; //TODO: Find length according to N.
	
	wire[3:0] sel_neg;
	
	not(sel_neg[0], sel[0]),
	   (sel_neg[1], sel[1]),
	   (sel_neg[2], sel[2]),
	   (sel_neg[3], sel[3]);
	
	and(out[0], sel_neg[3], sel_neg[2], sel_neg[1], sel_neg[0], in),
	   (out[1], sel_neg[3], sel_neg[2], sel_neg[1], sel[0], in),
	   (out[2], sel_neg[3], sel_neg[2], sel[1], sel_neg[0], in),
	   (out[3], sel_neg[3], sel_neg[2], sel[1], sel[0], in),
	   (out[4], sel_neg[3], sel[2], sel_neg[1], sel_neg[0], in),
	   (out[5], sel_neg[3], sel[2], sel_neg[1], sel[0], in),
	   (out[6], sel_neg[3], sel[2], sel[1], sel_neg[0], in),
	   (out[7], sel_neg[3], sel[2], sel[1], sel[0], in),
	   (out[8], sel[3], sel_neg[2], sel_neg[1], sel_neg[0], in),
	   (out[9], sel[3], sel_neg[2], sel_neg[1], sel[0], in),
	   (out[10], sel[3], sel_neg[2], sel[1], sel_neg[0], in),
	   (out[11], sel[3], sel_neg[2], sel[1], sel[0], in),
	   (out[12], sel[3], sel[2], sel_neg[1], sel_neg[0], in),
	   (out[13], sel[3], sel[2], sel_neg[1], sel[0], in),
	   (out[14], sel[3], sel[2], sel[1], sel_neg[0], in),
	   (out[15], sel[3], sel[2], sel[1], sel[0], in);
	
endmodule

/*
* @module Demux_1_32_SDF
* @brief Demultiplexer 1:32, with a gate level of abstraction.
* @output out Array of outputs.
* @input in Single input.
* @input sel Selector of output(in the form of an array of bits).
*/
module Demux_1_32_SDF(out, in, sel);

	parameter N = 32;
	
	output[N-1:0] out;
	input in;
	input[4:0] sel; //TODO: Find length according to N.
	
	wire[4:0] sel_neg;
	
	not(sel_neg[0], sel[0]),
	   (sel_neg[1], sel[1]),
	   (sel_neg[2], sel[2]),
	   (sel_neg[3], sel[3]),
	   (sel_neg[4], sel[4]);
	
	and(out[0], sel_neg[4], sel_neg[3], sel_neg[2], sel_neg[1], sel_neg[0], in),
	   (out[1], sel_neg[4], sel_neg[3], sel_neg[2], sel_neg[1], sel[0], in),
	   (out[2], sel_neg[4], sel_neg[3], sel_neg[2], sel[1], sel_neg[0], in),
	   (out[3], sel_neg[4], sel_neg[3], sel_neg[2], sel[1], sel[0], in),
	   (out[4], sel_neg[4], sel_neg[3], sel[2], sel_neg[1], sel_neg[0], in),
	   (out[5], sel_neg[4], sel_neg[3], sel[2], sel_neg[1], sel[0], in),
	   (out[6], sel_neg[4], sel_neg[3], sel[2], sel[1], sel_neg[0], in),
	   (out[7], sel_neg[4], sel_neg[3], sel[2], sel[1], sel[0], in),
	   (out[8], sel_neg[4], sel[3], sel_neg[2], sel_neg[1], sel_neg[0], in),
	   (out[9], sel_neg[4], sel[3], sel_neg[2], sel_neg[1], sel[0], in),
	   (out[10], sel_neg[4], sel[3], sel_neg[2], sel[1], sel_neg[0], in),
	   (out[11], sel_neg[4], sel[3], sel_neg[2], sel[1], sel[0], in),
	   (out[12], sel_neg[4], sel[3], sel[2], sel_neg[1], sel_neg[0], in),
	   (out[13], sel_neg[4], sel[3], sel[2], sel_neg[1], sel[0], in),
	   (out[14], sel_neg[4], sel[3], sel[2], sel[1], sel_neg[0], in),
	   (out[15], sel_neg[4], sel[3], sel[2], sel[1], sel[0], in),
	   (out[16], sel[4], sel_neg[3], sel_neg[2], sel_neg[1], sel_neg[0], in),
	   (out[17], sel[4], sel_neg[3], sel_neg[2], sel_neg[1], sel[0], in),
	   (out[18], sel[4], sel_neg[3], sel_neg[2], sel[1], sel_neg[0], in),
	   (out[19], sel[4], sel_neg[3], sel_neg[2], sel[1], sel[0], in),
	   (out[20], sel[4], sel_neg[3], sel[2], sel_neg[1], sel_neg[0], in),
	   (out[21], sel[4], sel_neg[3], sel[2], sel_neg[1], sel[0], in),
	   (out[22], sel[4], sel_neg[3], sel[2], sel[1], sel_neg[0], in),
	   (out[23], sel[4], sel_neg[3], sel[2], sel[1], sel[0], in),
	   (out[24], sel[4], sel[3], sel_neg[2], sel_neg[1], sel_neg[0], in),
	   (out[25], sel[4], sel[3], sel_neg[2], sel_neg[1], sel[0], in),
	   (out[26], sel[4], sel[3], sel_neg[2], sel[1], sel_neg[0], in),
	   (out[27], sel[4], sel[3], sel_neg[2], sel[1], sel[0], in),
	   (out[28], sel[4], sel[3], sel[2], sel_neg[1], sel_neg[0], in),
	   (out[29], sel[4], sel[3], sel[2], sel_neg[1], sel[0], in),
	   (out[30], sel[4], sel[3], sel[2], sel[1], sel_neg[0], in),
	   (out[31], sel[4], sel[3], sel[2], sel[1], sel[0], in);
	
endmodule
