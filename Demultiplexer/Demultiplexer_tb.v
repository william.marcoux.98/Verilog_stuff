`include "Demultiplexer.v"

module main_tb();

	reg[2:0] sel;
	wire[7:0] result;
	
	Demux_1_8_SDF u0(result, 1'b1, sel);
	
	initial begin
		$dumpfile("Demux_test.vcd");
		$dumpvars(0, main_tb);
		#5 sel = 3'd0;
		#5 sel = 3'd1;
		#5 sel = 3'd2;
		#5 sel = 3'd3;
		#5 sel = 3'd4;
		#5 sel = 3'd5;
		#5 sel = 3'd6;
		#5 sel = 3'd7;
		#5 $finish;
	end

endmodule