/**
* @file PriorityEncoder.v
* @brief File describing priority encoders on different abstraction levels and with different magnitude capacities
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date January 14th, 2018
*
* This file has been written for two reasons:
*	a) Putting in practice what I learned about Verilog
*	b) Making my portfolio bigger
*/

/*
* @module PriorityEncoder_4_2_RTL
* @brief Priority encoder 4:2, with register transfer level of abstraction.
* @output out Output bits.
* @input in Input bits.
*/
module PriorityEncoder_4_2_RTL(out, in);

	output reg[1:0] out;
	input[3:0] in;
	
	always @(in or out)
		if(in[3]) begin
			assign out = 2'b11;
		end else if(in[2]) begin
			assign out = 2'b10;
		end else if(in[1]) begin
			assign out = 2'b01;
		end else if(in[0]) begin
			assign out = 2'b00;
		end
	
endmodule

/*
* @module PriorityDecoder_2_4_RTL
* @brief Priority decoder 2:4, with register transfer level of abstraction.
* @output out Output bits.
* @input in Input bits.
*/
module PriorityDecoder_2_4_RTL(out, in);

	output reg[3:0] out;
	input[1:0] in;
	
	always @(in or out)
		case(in)
			0: assign out = 4'b0001;
			1: assign out = 4'b0010;
			2: assign out = 4'b0100;
			3: assign out = 4'b1000;
		endcase
	
endmodule

/*
* @module PriorityEncoder_8_3_RTL
* @brief Priority encoder 8:3, with register transfer level of abstraction.
* @output out Output bits.
* @input in Input bits.
*/
module PriorityEncoder_8_3_RTL(out, in);

	output reg[2:0] out;
	input[7:0] in;
	
	always @(in or out)
		if(in[7]) begin
			assign out = 3'd7;
		end else if(in[6]) begin
			assign out = 3'd6;
		end else if(in[5]) begin
			assign out = 3'd5;
		end else if(in[4]) begin
			assign out = 3'd4;
		end else if(in[3]) begin
			assign out = 3'd3;
		end else if(in[2]) begin
			assign out = 3'd2;
		end else if(in[1]) begin
			assign out = 3'd1;
		end else if(in[0]) begin
			assign out = 3'd0;
		end
	
endmodule

/*
* @module PriorityDecoder_3_8_RTL
* @brief Priority decoder 3:8, with register transfer level of abstraction.
* @output out Output bits.
* @input in Input bits.
*/
module PriorityDecoder_3_8_RTL(out, in);

	output reg[7:0] out;
	input[2:0] in;
	
	always @(in or out)
		case(in)
			0: assign out = 8'b00000001;
			1: assign out = 8'b00000010;
			2: assign out = 8'b00000100;
			3: assign out = 8'b00001000;
			4: assign out = 8'b00010000;
			5: assign out = 8'b00100000;
			6: assign out = 8'b01000000;
			7: assign out = 8'b10000000;
		endcase
	
endmodule

/*
* @module PriorityEncoder_4_2_SDF
* @brief Priority encoder 4:2, with gate level of abstraction.
* @output out Output bits.
* @input in Input bits.
*/
module PriorityEncoder_4_2_SDF(out, in);

	output[1:0] out;
	input[3:0] in;
	
	wire[1:0] nets;
	
	not(nets[0], in[2]);
	
	and(nets[1], nets[0], in[1]);
	
	or(out[0], nets[1], in[3]),
	  (out[1], in[2], in[3]);
	
endmodule

/*
* @module PriorityDecoder_2_4_SDF
* @brief Priority decoder 2:4, with gate level of abstraction.
* @output out Output bits.
* @input in Input bits.
*/
module PriorityDecoder_2_4_SDF(out, in);

	output[3:0] out;
	input[1:0] in;
	
	wire[1:0] in_neg;
	
	not(in_neg[0], in[0]),
	   (in_neg[1], in[1]);
	   
	and(out[0], in_neg[1], in_neg[0]),
	   (out[1], in_neg[1], in[0]),
	   (out[2], in[1], in_neg[0]),
	   (out[3], in[1], in[0]);
	
endmodule

/*
* @module PriorityEncoder_8_3_SDF
* @brief Priority encoder 8:3, with gate level of abstraction.
* @output out Output bits.
* @input in Input bits.
*/
module PriorityEncoder_8_3_SDF(out, in);

	output[2:0] out;
	input[7:0] in;
	
	or(out[0], in[1], in[3], in[5], in[7]),
	  (out[1], in[2], in[3], in[6], in[7]),
	  (out[2], in[4], in[5], in[6], in[7]);
	
endmodule

/*
* @module PriorityDecoder_3_8_SDF
* @brief Priority decoder 3:8, with gate level of abstraction.
* @output out Output bits.
* @input in Input bits.
*/
module PriorityDecoder_3_8_SDF(out, in);

	output[7:0] out;
	input[2:0] in;
	
	wire[2:0] in_neg;
	
	not(in_neg[0], in[0]),
	   (in_neg[1], in[1]),
	   (in_neg[2], in[2]);
	
	and(out[0], in_neg[0], in_neg[1], in_neg[2]),
	   (out[1], in_neg[0], in_neg[1], in[2]),
	   (out[2], in_neg[0], in[1], in_neg[2]),
	   (out[3], in_neg[0], in[1], in[2]),
	   (out[4], in[0], in_neg[1], in_neg[2]),
	   (out[5], in[0], in_neg[1], in[2]),
	   (out[6], in[0], in[1], in_neg[2]),
	   (out[7], in[0], in[1], in[2]);
	
endmodule



