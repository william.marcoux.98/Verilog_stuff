/**
* @file BinaryAdder.v
* @brief File describing binary adders on different abstraction levels and with different magnitude capacities
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date January 4th, 2018
*
* This file has been written for two reasons:
*	a) Putting in practice what I learned about Verilog
*	b) Making my portfolio bigger
*/

`include "HalfAdder.v"
`include "FullAdder.v"

/*
* @module BinaryAdder_4b_RTL_Chain
* @brief Binary adder for 4 bits numbers, chainable (to put other adding blocks before), with register transfer level of abstraction.
* @output carry_out Carry bit resulting from the summation of both addends.
* @output[4] sum Sum of the two input addends.
* @input carry_in Carry bit resulting from the sum of a previous adding block.
* @input[4] addend1 First addend of the summation.
* @input[4] addend2 Second addend of the summation.
*/
module BinaryAdder_4b_RTL_Chain(
output carry_out,
output[3:0] sum,
input carry_in,
input[3:0] addend1,
input[3:0] addend2);

	assign {carry_out, sum[3], sum[2], sum[1], sum[0]} = carry_in + addend1 + addend2;

endmodule

/*
* @module BinaryAdder_4b_RTL
* @brief Binary adder for 4 bits numbers, with register transfer level of abstraction.
* @output carry_out Carry bit resulting from the summation of both addends.
* @output[4] sum Sum of the two input addends.
* @input[4] addend1 First addend of the summation.
* @input[4] addend2 Second addend of the summation.
*/
module BinaryAdder_4b_RTL(
output carry_out,
output[3:0] sum,
input[3:0] addend1,
input[3:0] addend2);

	assign {carry_out, sum[3], sum[2], sum[1], sum[0]} = addend1 + addend2;

endmodule

/*
* @module BinaryAdder_8b_RTL_Chain
* @brief Binary adder for 8 bits numbers, chainable (to put other adding blocks before), with register transfer level of abstraction.
* @output carry_out Carry bit resulting from the summation of both addends.
* @output[8] sum Sum of the two input addends.
* @input carry_in Carry bit resulting from the sum of a previous adding block.
* @input[8] addend1 First addend of the summation.
* @input[8] addend2 Second addend of the summation.
*/
module BinaryAdder_8b_RTL_Chain(
output carry_out,
output[7:0] sum,
input carry_in,
input[7:0] addend1,
input[7:0] addend2);

	assign {carry_out, sum[7], sum[6], sum[5], sum[4], sum[3], sum[2], sum[1], sum[0]} = carry_in + addend1 + addend2;

endmodule

/*
* @module BinaryAdder_8b_RTL
* @brief Binary adder for 8 bits numbers, with register transfer level of abstraction.
* @output carry_out Carry bit resulting from the summation of both addends.
* @output[8] sum Sum of the two input addends.
* @input[8] addend1 First addend of the summation.
* @input[8] addend2 Second addend of the summation.
*/
module BinaryAdder_8b_RTL(
output carry_out,
output[7:0] sum,
input[7:0] addend1,
input[7:0] addend2);

	assign {carry_out, sum[7], sum[6], sum[5], sum[4], sum[3], sum[2], sum[1], sum[0]} = addend1 + addend2;

endmodule

/*
* @module BinaryAdder_16b_RTL_Chain
* @brief Binary adder for 16 bits numbers, chainable (to put other adding blocks before), with register transfer level of abstraction.
* @output carry_out Carry bit resulting from the summation of both addends.
* @output[16] sum Sum of the two input addends.
* @input carry_in Carry bit resulting from the sum of a previous adding block.
* @input[16] addend1 First addend of the summation.
* @input[16] addend2 Second addend of the summation.
*/
module BinaryAdder_16b_RTL_Chain(
output carry_out,
output[15:0] sum,
input carry_in,
input[15:0] addend1,
input[15:0] addend2);

	assign {carry_out, sum[15], sum[14], sum[13], sum[12], sum[11], sum[10], sum[9], sum[8]
					 , sum[7], sum[6], sum[5], sum[4], sum[3], sum[2], sum[1], sum[0]} = carry_in + addend1 + addend2;

endmodule

/*
* @module BinaryAdder_16b_RTL
* @brief Binary adder for 16 bits numbers, with register transfer level of abstraction.
* @output carry_out Carry bit resulting from the summation of both addends.
* @output[16] sum Sum of the two input addends.
* @input[16] addend1 First addend of the summation.
* @input[16] addend2 Second addend of the summation.
*/
module BinaryAdder_16b_RTL(
output carry_out,
output[15:0] sum,
input[15:0] addend1,
input[15:0] addend2);

	assign {carry_out, sum[15], sum[14], sum[13], sum[12], sum[11], sum[10], sum[9], sum[8]
					 , sum[7], sum[6], sum[5], sum[4], sum[3], sum[2], sum[1], sum[0]} = addend1 + addend2;

endmodule

/*
* @module BinaryAdder_4b_SDF_Chain
* @brief Binary adder for 4 bits numbers, chainable (to put other adding blocks before), with gate level of abstraction.
* @output carry_out Carry bit resulting from the summation of both addends.
* @output[4] sum Sum of the two input addends.
* @input carry_in Carry bit resulting from the sum of a previous adding block.
* @input[4] addend1 First addend of the summation.
* @input[4] addend2 Second addend of the summation.
*/
module BinaryAdder_4b_SDF_Chain(
output carry_out,
output[3:0] sum,
input carry_in,
input[3:0] addend1,
input[3:0] addend2);

	wire[2:0] nets;
	
	FullAdder_SDF u0(sum[0], nets[0], carry_in, addend1[0], addend2[0]);
	FullAdder_SDF u1(sum[1], nets[1], nets[0], addend1[1], addend2[1]);
	FullAdder_SDF u2(sum[2], nets[2], nets[1], addend1[2], addend2[2]);
	FullAdder_SDF u3(sum[3], carry_out, nets[2], addend1[3], addend2[3]);

endmodule

/*
* @module BinaryAdder_4b_SDF
* @brief Binary adder for 4 bits numbers, with gate level of abstraction.
* @output carry_out Carry bit resulting from the summation of both addends.
* @output[4] sum Sum of the two input addends.
* @input[4] addend1 First addend of the summation.
* @input[4] addend2 Second addend of the summation.
*/
module BinaryAdder_4b_SDF(
output carry_out,
output[3:0] sum,
input[3:0] addend1,
input[3:0] addend2);

	wire[2:0] nets;
	
	HalfAdder_SDF u0(sum[0], nets[0], addend1[0], addend2[0]);
	FullAdder_SDF u1(sum[1], nets[1], nets[0], addend1[1], addend2[1]);
	FullAdder_SDF u2(sum[2], nets[2], nets[1], addend1[2], addend2[2]);
	FullAdder_SDF u3(sum[3], carry_out, nets[2], addend1[3], addend2[3]);

endmodule

/*
* @module BinaryAdder_8b_SDF_Chain
* @brief Binary adder for 8 bits numbers, chainable (to put other adding blocks before), with gate level of abstraction.
* @output carry_out Carry bit resulting from the summation of both addends.
* @output[8] sum Sum of the two input addends.
* @input carry_in Carry bit resulting from the sum of a previous adding block.
* @input[8] addend1 First addend of the summation.
* @input[8] addend2 Second addend of the summation.
*/
module BinaryAdder_8b_SDF_Chain(
output carry_out,
output[7:0] sum,
input carry_in,
input[7:0] addend1,
input[7:0] addend2);

	wire nets;
	
	BinaryAdder_4b_SDF_Chain u0(nets, sum[3:0], carry_in, addend1[3:0], addend2[3:0]);
	BinaryAdder_4b_SDF_Chain u1(carry_out, sum[7:4], nets, addend1[7:4], addend2[7:4]);

endmodule

/*
* @module BinaryAdder_8b_SDF
* @brief Binary adder for 8 bits numbers, with gate level of abstraction.
* @output carry_out Carry bit resulting from the summation of both addends.
* @output[8] sum Sum of the two input addends.
* @input[8] addend1 First addend of the summation.
* @input[8] addend2 Second addend of the summation.
*/
module BinaryAdder_8b_SDF(
output carry_out,
output[7:0] sum,
input[7:0] addend1,
input[7:0] addend2);

	wire nets;
	
	BinaryAdder_4b_SDF u0(nets, sum[3:0], addend1[3:0], addend2[3:0]);
	BinaryAdder_4b_SDF_Chain u1(carry_out, sum[7:4], nets, addend1[7:4], addend2[7:4]);
	
endmodule

/*
* @module BinaryAdder_16b_SDF_Chain
* @brief Binary adder for 16 bits numbers, chainable (to put other adding blocks before), with gate level of abstraction.
* @output carry_out Carry bit resulting from the summation of both addends.
* @output[16] sum Sum of the two input addends.
* @input carry_in Carry bit resulting from the sum of a previous adding block.
* @input[16] addend1 First addend of the summation.
* @input[16] addend2 Second addend of the summation.
*/
module BinaryAdder_16b_SDF_Chain(
output carry_out,
output[15:0] sum,
input carry_in,
input[15:0] addend1,
input[15:0] addend2);

	wire nets;
	
	BinaryAdder_8b_SDF_Chain u0(nets, sum[7:0], carry_in, addend1[7:0], addend2[7:0]);
	BinaryAdder_8b_SDF_Chain u1(carry_out, sum[15:8], nets, addend1[15:8], addend2[15:8]);

endmodule

/*
* @module BinaryAdder_16b_SDF
* @brief Binary adder for 16 bits numbers, with gate level of abstraction.
* @output carry_out Carry bit resulting from the summation of both addends.
* @output[16] sum Sum of the two input addends.
* @input[16] addend1 First addend of the summation.
* @input[16] addend2 Second addend of the summation.
*/
module BinaryAdder_16b_SDF(
output carry_out,
output[15:0] sum,
input[15:0] addend1,
input[15:0] addend2);

	wire nets;
	
	BinaryAdder_8b_SDF u0(nets, sum[7:0], addend1[7:0], addend2[7:0]);
	BinaryAdder_8b_SDF_Chain u1(carry_out, sum[15:8], nets, addend1[15:8], addend2[15:8]);
	
endmodule

