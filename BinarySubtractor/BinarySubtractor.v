/**
* @file BinarySubtractor.v
* @brief File describing binary subtractors on different abstraction levels and with different magnitude capacities
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date January 4th, 2018
*
* This file has been written for two reasons:
*	a) Putting in practice what I learned about Verilog
*	b) Making my portfolio bigger
*
*	Note: This may not be a definitive version.
*/

`include "BinaryAdder.v"

module BinarySubtractor_4b_RTL_Chain(
output carry_out,
output[3:0] diff,
input carry_in,
input[3:0] minuend,
input[3:0] subtrahend);

	assign {carry_out, diff[3], diff[2], diff[1], diff[0]} = minuend - carry_in - subtrahend;

endmodule

module BinarySubtractor_4b_RTL(
output carry_out,
output[3:0] diff,
input[3:0] minuend,
input[3:0] subtrahend);

	assign {carry_out, diff[3], diff[2], diff[1], diff[0]} = minuend - subtrahend;

endmodule

module BinarySubtractor_8b_RTL_Chain(
output carry_out,
output[7:0] diff,
input carry_in,
input[7:0] minuend,
input[7:0] subtrahend);

	assign {carry_out, diff[7], diff[6], diff[5], diff[4],
					   diff[3], diff[2], diff[1], diff[0]} = minuend - carry_in - subtrahend;

endmodule

module BinarySubtractor_8b_RTL(
output carry_out,
output[7:0] diff,
input[7:0] minuend,
input[7:0] subtrahend);

	assign {carry_out, diff[7], diff[6], diff[5], diff[4],
					   diff[3], diff[2], diff[1], diff[0]} = minuend - subtrahend;

endmodule

module BinarySubtractor_16b_RTL_Chain(
output carry_out,
output[15:0] diff,
input carry_in,
input[15:0] minuend,
input[15:0] subtrahend);

	assign {carry_out, diff[15], diff[14], diff[13], diff[12],
					   diff[11], diff[10], diff[9], diff[8],
					   diff[7], diff[6], diff[5], diff[4],
					   diff[3], diff[2], diff[1], diff[0]} = minuend - carry_in - subtrahend;

endmodule

module BinarySubtractor_16b_RTL(
output carry_out,
output[15:0] diff,
input[15:0] minuend,
input[15:0] subtrahend);

	assign {carry_out, diff[15], diff[14], diff[13], diff[12],
					   diff[11], diff[10], diff[9], diff[8],
					   diff[7], diff[6], diff[5], diff[4],
					   diff[3], diff[2], diff[1], diff[0]} = minuend - subtrahend;

endmodule
//////////////////////////////////////////////

module BinarySubtractor_4b_SDF_Chain(
output carry_out,
output[3:0] diff,
input carry_in,
input[3:0] minuend,
input[3:0] subtrahend);

	wire[3:0] nets;
	
	not(nets[0], subtrahend[0]);
	not(nets[1], subtrahend[1]);
	not(nets[2], subtrahend[2]);
	not(nets[3], subtrahend[3]);

	BinaryAdder_4b_SDF_Chain ba4b0(carry_out, diff, carry_in, minuend, nets);	
	
endmodule

module BinarySubtractor_4b_SDF(
output carry_out,
output[3:0] diff,
input[3:0] minuend,
input[3:0] subtrahend);

	wire[3:0] nets;
	
	not(nets[0], subtrahend[0]);
	not(nets[1], subtrahend[1]);
	not(nets[2], subtrahend[2]);
	not(nets[3], subtrahend[3]);

	BinaryAdder_4b_SDF_Chain ba4b0(carry_out, diff, 1'b1, minuend, nets);	
	
endmodule

module BinarySubtractor_8b_SDF_Chain(
output carry_out,
output[7:0] diff,
input carry_in,
input[7:0] minuend,
input[7:0] subtrahend);

	wire[7:0] nets;
	
	not(nets[0], subtrahend[0]);
	not(nets[1], subtrahend[1]);
	not(nets[2], subtrahend[2]);
	not(nets[3], subtrahend[3]);
	not(nets[4], subtrahend[4]);
	not(nets[5], subtrahend[5]);
	not(nets[6], subtrahend[6]);
	not(nets[7], subtrahend[7]);

	BinaryAdder_8b_SDF_Chain ba8b0(carry_out, diff, carry_in, minuend, nets);	
	
endmodule

module BinarySubtractor_8b_SDF(
output carry_out,
output[7:0] diff,
input[7:0] minuend,
input[7:0] subtrahend);

	wire[7:0] nets;
	
	not(nets[0], subtrahend[0]);
	not(nets[1], subtrahend[1]);
	not(nets[2], subtrahend[2]);
	not(nets[3], subtrahend[3]);
	not(nets[4], subtrahend[4]);
	not(nets[5], subtrahend[5]);
	not(nets[6], subtrahend[6]);
	not(nets[7], subtrahend[7]);

	BinaryAdder_8b_SDF_Chain ba8b0(carry_out, diff, 1'b1, minuend, nets);	
	
endmodule

module BinarySubtractor_16b_SDF_Chain(
output carry_out,
output[15:0] diff,
input carry_in,
input[15:0] minuend,
input[15:0] subtrahend);

	wire[15:0] nets;
	
	not(nets[0], subtrahend[0]);
	not(nets[1], subtrahend[1]);
	not(nets[2], subtrahend[2]);
	not(nets[3], subtrahend[3]);
	not(nets[4], subtrahend[4]);
	not(nets[5], subtrahend[5]);
	not(nets[6], subtrahend[6]);
	not(nets[7], subtrahend[7]);
	not(nets[8], subtrahend[8]);
	not(nets[9], subtrahend[9]);
	not(nets[10], subtrahend[10]);
	not(nets[11], subtrahend[11]);
	not(nets[12], subtrahend[12]);
	not(nets[13], subtrahend[13]);
	not(nets[14], subtrahend[14]);
	not(nets[15], subtrahend[15]);

	BinaryAdder_16b_SDF_Chain ba16b0(carry_out, diff, carry_in, minuend, nets);	
	
endmodule

module BinarySubtractor_16b_SDF(
output carry_out,
output[15:0] diff,
input[15:0] minuend,
input[15:0] subtrahend);

	wire[15:0] nets;
	
	not(nets[0], subtrahend[0]);
	not(nets[1], subtrahend[1]);
	not(nets[2], subtrahend[2]);
	not(nets[3], subtrahend[3]);
	not(nets[4], subtrahend[4]);
	not(nets[5], subtrahend[5]);
	not(nets[6], subtrahend[6]);
	not(nets[7], subtrahend[7]);
	not(nets[8], subtrahend[8]);
	not(nets[9], subtrahend[9]);
	not(nets[10], subtrahend[10]);
	not(nets[11], subtrahend[11]);
	not(nets[12], subtrahend[12]);
	not(nets[13], subtrahend[13]);
	not(nets[14], subtrahend[14]);
	not(nets[15], subtrahend[15]);

	BinaryAdder_16b_SDF_Chain ba16b0(carry_out, diff, 1'b1, minuend, nets);	
	
endmodule

