/**
* @file FullAdder.v
* @brief File describing binary full adders on different abstraction levels
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date January 4th, 2018
*
* This file has been written for two reasons:
*	a) Putting in practice what I learned about Verilog
*	b) Making my portfolio bigger
*/


/**
* @module FullAdder_RTL
* @brief Binary full adder with a register transfer level of abstraction.
* @output sum Sum bit of the som of the two input bits.
* @output carry_out Carry bit of the sum of the two input bits.
* @input carry_in Carry bit resulting from the sum of a previous adding block.
* @input a First addend.
* @input b Second addend.
*/
module FullAdder_RTL(
output sum, 
output carry_out,
input carry_in,
input a,
input b);
	
	assign {carry_out, sum} = carry_in + a + b;

	//This was the previous strategy. I found another, slicker one.
	//assign carry_out = (a & b) | (carry_in & (a ^ b));
	//assign sum = carry_in ^ (a ^ b);
	
endmodule

/**
* @module FullAdder_SDF
* @brief Binary full adder with a gate level of abstraction.
* @output sum Sum bit of the som of the two input bits.
* @output carry_out Carry bit of the sum of the two input bits.
* @input carry_in Carry bit resulting from the sum of a previous adding block.
* @input a First addend.
* @input b Second addend.
*/
module FullAdder_SDF(
output sum, 
output carry_out,
input carry_in,
input a,
input b);

	wire[2:0] nets;

	xor(nets[0], a, b);
	xor(sum, nets[0], carry_in);
	and(nets[1], nets[0], carry_in);
	and(nets[2], a, b);
	or(carry_out, nets[2], nets[1]);
	
endmodule

/**
* @module FullAdder_TTab
* @brief Binary full adder using truth tables in primitives.
* @output sum Sum bit of the som of the two input bits.
* @output carry_out Carry bit of the sum of the two input bits.
* @input carry_in Carry bit resulting from the sum of a previous adding block.
* @input a First addend.
* @input b Second addend.
*/
module FullAdder_TTab(
output sum, 
output carry_out,
input carry_in,
input a,
input b);

	FullAdder_sum(sum, carry_in, a, b);
	FullAdder_carry_out(carry_out, carry_in, a, b);
	
endmodule

/**
* @primitive FullAdder_sum
* @brief Primitive expressing the truth table for the sum output of the full adder
* @output sum Sum bit of the som of the two input bits.
* @input carry_in Carry bit resulting from the sum of a previous adding block.
* @input a First addend.
* @input b Second addend.
*/
primitive FullAdder_sum(
output sum, 
input carry_in,
input a,
input b);

	table
		0 0 0 : 0;
		0 0 1 : 1;
		0 1 0 : 1;
		0 1 1 : 0;
		1 0 0 : 1;
		1 0 1 : 0;
		1 1 0 : 0;
		1 1 1 : 1;
	endtable

endprimitive

/**
* @primitive FullAdder_carry_out
* @brief Primitive expressing the truth table for the carry_out output of the full adder.
* @output carry_out Carry bit of the sum of the two input bits.
* @input carry_in Carry bit resulting from the sum of a previous adding block.
* @input a First addend.
* @input b Second addend.
*/
primitive FullAdder_carry_out(
output carry_out, 
input carry_in,
input a,
input b);

	table
		0 0 0 : 0;
		0 0 1 : 0;
		0 1 0 : 0;
		0 1 1 : 1;
		1 0 0 : 0;
		1 0 1 : 1;
		1 1 0 : 1;
		1 1 1 : 1;
	endtable

endprimitive