`include "BinarySubtractor.v"

module main_tb();

	wire carry_out;
	wire[3:0] diff;
	
	BinarySubtractor_4b_SDF_Chain bs0(carry_out, diff, 1'b1, 4'd7, 4'd4);
	
	initial begin
		$dumpfile("BinarySubtractor_test.vcd");
		$dumpvars(0, main_tb);

		#5 $finish;
	end

endmodule