/**
* @file Multiplexer.v
* @brief File describing various multiplexers on different abstraction levels.
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date January 13th, 2018
*
* This file has been written for two reasons:
*	a) Putting in practice what I learned about Verilog
*	b) Making my portfolio bigger
*/

/*
* @module Mux_2_1_RTL
* @brief Multiplexer 2:1, with register transfer level of abstraction.
* @output out Bit resulting from inputs and selectors.
* @input in Array of bits corresponding to the input.
* @input sel Array of bits corresponding to the selector.
*/
module Mux_2_1_RTL(out, in, sel);

	parameter N = 2;
	
	output out;
	input[N-1:0] in;
	input sel;
	
	assign out = in[sel];
	
endmodule

/*
* @module Mux_4_1_RTL
* @brief Multiplexer 4:1, with register transfer level of abstraction.
* @output out Bit resulting from inputs and selectors.
* @input in Array of bits corresponding to the input.
* @input sel Array of bits corresponding to the selector.
*/
module Mux_4_1_RTL(out, in, sel);

	parameter N = 4;
	
	output out;
	input[N-1:0] in;
	input[1:0] sel;
	
	assign out = in[sel];
	
endmodule

/*
* @module Mux_8_1_RTL
* @brief Multiplexer 8:1, with register transfer level of abstraction.
* @output out Bit resulting from inputs and selectors.
* @input in Array of bits corresponding to the input.
* @input sel Array of bits corresponding to the selector.
*/
module Mux_8_1_RTL(out, in, sel);

	parameter N = 8;
	
	output out;
	input[N-1:0] in;
	input[2:0] sel;
	
	assign out = in[sel];
	
endmodule

/*
* @module Mux_16_1_RTL
* @brief Multiplexer 16:1, with register transfer level of abstraction.
* @output out Bit resulting from inputs and selectors.
* @input in Array of bits corresponding to the input.
* @input sel Array of bits corresponding to the selector.
*/
module Mux_16_1_RTL(out, in, sel);

	parameter N = 16;
	
	output out;
	input[N-1:0] in;
	input[3:0] sel;
	
	assign out = in[sel];

endmodule

/*
* @module Mux_32_1_RTL
* @brief Multiplexer 32:1, with register transfer level of abstraction.
* @output out Bit resulting from inputs and selectors.
* @input in Array of bits corresponding to the input.
* @input sel Array of bits corresponding to the selector.
*/
module Mux_32_1_RTL(out, in, sel);

	parameter N = 32;
	
	output out;
	input[N-1:0] in;
	input[4:0] sel;
	
	assign out = in[sel];

endmodule

/*
* @module Mux_64_1_RTL
* @brief Multiplexer 64:1, with register transfer level of abstraction.
* @output out Bit resulting from inputs and selectors.
* @input in Array of bits corresponding to the input.
* @input sel Array of bits corresponding to the selector.
*/
module Mux_64_1_RTL(out, in, sel);

	parameter N = 64;
	
	output out;
	input[N-1:0] in;
	input[5:0] sel;
	
	assign out = in[sel];

endmodule

/*
* @module Mux_2_1_SDF
* @brief Multiplexer 2:1, with a gate level of abstraction.
* @output out Bit resulting from inputs and selectors.
* @input in Array of bits corresponding to the input.
* @input sel Array of bits corresponding to the selector.
*/
module Mux_2_1_SDF(out, in, sel);

	parameter N = 2;
	
	output out;
	input[N-1:0] in;
	input sel;
	
	wire sel_neg;
	wire[N-1:0] and_nets;
	
	not(sel_neg, sel);
	
	and(and_nets[0], in[0], sel_neg),
	   (and_nets[1], in[1], sel);
	   
	or(out, and_nets[0], and_nets[1]);
	
endmodule

/*
* @module Mux_4_1_SDF
* @brief Multiplexer 4:1, with a gate level of abstraction.
* @output out Bit resulting from inputs and selectors.
* @input in Array of bits corresponding to the input.
* @input sel Array of bits corresponding to the selector.
*/
module Mux_4_1_SDF(out, in, sel);

	parameter N = 4;
	
	output out;
	input[N-1:0] in;
	input[1:0] sel;
	
	wire[1:0] sel_neg;
	wire[N-1:0] and_nets;
	
	not(sel_neg[0], sel[0]),
	   (sel_neg[1], sel[1]);
	
	and(and_nets[0], in[0], sel_neg[1], sel_neg[0]),
	   (and_nets[1], in[1], sel_neg[1], sel[0]),
	   (and_nets[2], in[2], sel[1], sel_neg[0]),
	   (and_nets[3], in[3], sel[1], sel[0]);
	   
	or(out, and_nets[0], and_nets[1], and_nets[2], and_nets[3]);
	
endmodule

/*
* @module Mux_8_1_SDF
* @brief Multiplexer 8:1, with a gate level of abstraction.
* @output out Bit resulting from inputs and selectors.
* @input in Array of bits corresponding to the input.
* @input sel Array of bits corresponding to the selector.
*/
module Mux_8_1_SDF(out, in, sel);

	parameter N = 8;
	
	output out;
	input[N-1:0] in;
	input[2:0] sel;
	
	wire[2:0] sel_neg;
	wire[N-1:0] and_nets;
	
	not(sel_neg[0], sel[0]),
	   (sel_neg[1], sel[1]),
	   (sel_neg[2], sel[2]);
	
	and(and_nets[0], in[0], sel_neg[2], sel_neg[1], sel_neg[0]),
	   (and_nets[1], in[1], sel_neg[2], sel_neg[1], sel[0]),
	   (and_nets[2], in[2], sel_neg[2], sel[1], sel_neg[0]),
	   (and_nets[3], in[3], sel_neg[2], sel[1], sel[0]),
	   (and_nets[4], in[4], sel[2], sel_neg[1], sel_neg[0]),
	   (and_nets[5], in[5], sel[2], sel_neg[1], sel[0]),
	   (and_nets[6], in[6], sel[2], sel[1], sel_neg[0]),
	   (and_nets[7], in[7], sel[2], sel[1], sel[0]);
	   
	or(out, and_nets[0], and_nets[1], and_nets[2], and_nets[3],
			and_nets[4], and_nets[5], and_nets[6], and_nets[7]);
	
endmodule

/*
* @module Mux_16_1_SDF
* @brief Multiplexer 16:1, with a gate level of abstraction.
* @output out Bit resulting from inputs and selectors.
* @input in Array of bits corresponding to the input.
* @input sel Array of bits corresponding to the selector.
*/
module Mux_16_1_SDF(out, in, sel);

	parameter N = 16;
	
	output out;
	input[N-1:0] in;
	input[3:0] sel;
	
	wire[3:0] sel_neg;
	wire[N-1:0] and_nets;
	
	not(sel_neg[0], sel[0]),
	   (sel_neg[1], sel[1]),
	   (sel_neg[2], sel[2]),
	   (sel_neg[3], sel[3]);
	
	and(and_nets[0], in[0], sel_neg[3], sel_neg[2], sel_neg[1], sel_neg[0]),
	   (and_nets[1], in[1], sel_neg[3], sel_neg[2], sel_neg[1], sel[0]),
	   (and_nets[2], in[2], sel_neg[3], sel_neg[2], sel[1], sel_neg[0]),
	   (and_nets[3], in[3], sel_neg[3], sel_neg[2], sel[1], sel[0]),
	   (and_nets[4], in[4], sel_neg[3], sel[2], sel_neg[1], sel_neg[0]),
	   (and_nets[5], in[5], sel_neg[3], sel[2], sel_neg[1], sel[0]),
	   (and_nets[6], in[6], sel_neg[3], sel[2], sel[1], sel_neg[0]),
	   (and_nets[7], in[7], sel_neg[3], sel[2], sel[1], sel[0]),
	   (and_nets[8], in[8], sel[3], sel_neg[2], sel_neg[1], sel_neg[0]),
	   (and_nets[9], in[9], sel[3], sel_neg[2], sel_neg[1], sel[0]),
	   (and_nets[10], in[10], sel[3], sel_neg[2], sel[1], sel_neg[0]),
	   (and_nets[11], in[11], sel[3], sel_neg[2], sel[1], sel[0]),
	   (and_nets[12], in[12], sel[3], sel[2], sel_neg[1], sel_neg[0]),
	   (and_nets[13], in[13], sel[3], sel[2], sel_neg[1], sel[0]),
	   (and_nets[14], in[14], sel[3], sel[2], sel[1], sel_neg[0]),
	   (and_nets[15], in[15], sel[3], sel[2], sel[1], sel[0]);
	   
	or(out, and_nets[0], and_nets[1], and_nets[2], and_nets[3],
			and_nets[4], and_nets[5], and_nets[6], and_nets[7],
			and_nets[8], and_nets[9], and_nets[10], and_nets[11],
			and_nets[12], and_nets[13], and_nets[14], and_nets[15]);
	
endmodule

/*
* @module Mux_32_1_SDF
* @brief Multiplexer 32:1, with a gate level of abstraction.
* @output out Bit resulting from inputs and selectors.
* @input in Array of bits corresponding to the input.
* @input sel Array of bits corresponding to the selector.
*/
module Mux_32_1_SDF(out, in, sel);

	parameter N = 32;
	
	output out;
	input[N-1:0] in;
	input[4:0] sel;
	
	wire[4:0] sel_neg;
	wire[N-1:0] and_nets;
	
	not(sel_neg[0], sel[0]),
	   (sel_neg[1], sel[1]),
	   (sel_neg[2], sel[2]),
	   (sel_neg[3], sel[3]),
	   (sel_neg[4], sel[4]);
	
	and(and_nets[0], in[0], sel_neg[4], sel_neg[3], sel_neg[2], sel_neg[1], sel_neg[0]),
	   (and_nets[1], in[1], sel_neg[4], sel_neg[3], sel_neg[2], sel_neg[1], sel[0]),
	   (and_nets[2], in[2], sel_neg[4], sel_neg[3], sel_neg[2], sel[1], sel_neg[0]),
	   (and_nets[3], in[3], sel_neg[4], sel_neg[3], sel_neg[2], sel[1], sel[0]),
	   (and_nets[4], in[4], sel_neg[4], sel_neg[3], sel[2], sel_neg[1], sel_neg[0]),
	   (and_nets[5], in[5], sel_neg[4], sel_neg[3], sel[2], sel_neg[1], sel[0]),
	   (and_nets[6], in[6], sel_neg[4], sel_neg[3], sel[2], sel[1], sel_neg[0]),
	   (and_nets[7], in[7], sel_neg[4], sel_neg[3], sel[2], sel[1], sel[0]),
	   (and_nets[8], in[8], sel_neg[4], sel[3], sel_neg[2], sel_neg[1], sel_neg[0]),
	   (and_nets[9], in[9], sel_neg[4], sel[3], sel_neg[2], sel_neg[1], sel[0]),
	   (and_nets[10], in[10], sel_neg[4], sel[3], sel_neg[2], sel[1], sel_neg[0]),
	   (and_nets[11], in[11], sel_neg[4], sel[3], sel_neg[2], sel[1], sel[0]),
	   (and_nets[12], in[12], sel_neg[4], sel[3], sel[2], sel_neg[1], sel_neg[0]),
	   (and_nets[13], in[13], sel_neg[4], sel[3], sel[2], sel_neg[1], sel[0]),
	   (and_nets[14], in[14], sel_neg[4], sel[3], sel[2], sel[1], sel_neg[0]),
	   (and_nets[15], in[15], sel_neg[4], sel[3], sel[2], sel[1], sel[0]),
	   (and_nets[16], in[16], sel[4], sel_neg[3], sel_neg[2], sel_neg[1], sel_neg[0]),
	   (and_nets[17], in[17], sel[4], sel_neg[3], sel_neg[2], sel_neg[1], sel[0]),
	   (and_nets[18], in[18], sel[4], sel_neg[3], sel_neg[2], sel[1], sel_neg[0]),
	   (and_nets[19], in[19], sel[4], sel_neg[3], sel_neg[2], sel[1], sel[0]),
	   (and_nets[20], in[20], sel[4], sel_neg[3], sel[2], sel_neg[1], sel_neg[0]),
	   (and_nets[21], in[21], sel[4], sel_neg[3], sel[2], sel_neg[1], sel[0]),
	   (and_nets[22], in[22], sel[4], sel_neg[3], sel[2], sel[1], sel_neg[0]),
	   (and_nets[23], in[23], sel[4], sel_neg[3], sel[2], sel[1], sel[0]),
	   (and_nets[24], in[24], sel[4], sel[3], sel_neg[2], sel_neg[1], sel_neg[0]),
	   (and_nets[25], in[25], sel[4], sel[3], sel_neg[2], sel_neg[1], sel[0]),
	   (and_nets[26], in[26], sel[4], sel[3], sel_neg[2], sel[1], sel_neg[0]),
	   (and_nets[27], in[27], sel[4], sel[3], sel_neg[2], sel[1], sel[0]),
	   (and_nets[28], in[28], sel[4], sel[3], sel[2], sel_neg[1], sel_neg[0]),
	   (and_nets[29], in[29], sel[4], sel[3], sel[2], sel_neg[1], sel[0]),
	   (and_nets[30], in[30], sel[4], sel[3], sel[2], sel[1], sel_neg[0]),
	   (and_nets[31], in[31], sel[4], sel[3], sel[2], sel[1], sel[0]);
	   
	or(out, and_nets[0], and_nets[1], and_nets[2], and_nets[3],
			and_nets[4], and_nets[5], and_nets[6], and_nets[7],
			and_nets[8], and_nets[9], and_nets[10], and_nets[11],
			and_nets[12], and_nets[13], and_nets[14], and_nets[15],
			and_nets[16], and_nets[17], and_nets[18], and_nets[19],
			and_nets[20], and_nets[21], and_nets[22], and_nets[23],
			and_nets[24], and_nets[25], and_nets[26], and_nets[27],
			and_nets[28], and_nets[29], and_nets[30], and_nets[31]);

endmodule
