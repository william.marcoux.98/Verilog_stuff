@echo off &SETLOCAL

rem @file iverilogCompiler.bat
rem @brief Compiles many file for Icarus Verilog, with an option for gtkwave file
rem @author William Marcoux (mailto: william.marcoux.98@gmail.com)
rem @version 1.0.0
rem @date December 30th, 2017

rem This file is just meant to simplify the compiling
rem of Icarus Verilog files through command lines. It
rem has an option for gtkwave compiling. 

echo.

:fileListing
for %%f in (%*) do (
	call set "Myvar= %%Myvar%% %%~nf.v"
)

:iverilogGenerating

set filesList=%Myvar:~2%
set /p outputFile=Type the name of the output file:
echo %filesList%
iverilog -o %outputFile% %filesList%


:vvpGenerating
set /p vvpAction=Do you want to generate the waveforms? [y/n]:

if x%vvpAction:y=%==x%vvpAction% goto end
vvp -v >nul 2>&1 && (
	vvp %outputFile%
) || (
   echo vvp.exe cannot be found on system path.
)

:end
echo.

pause

:eof