`include "MagnitudeComparator.v"

module BinaryComparator_4b_RTL(equal, lower, greater, num1, num2);
	
	parameter N = 4;
	
	output equal, lower, greater;
	input[N-1:0] num1, num2;
	
	assign equal   = (num1 == num2);
	assign lower   = (num1 < num2);
	assign greater = (num1 > num2);

endmodule

module BinaryComparator_8b_RTL(equal, lower, greater, num1, num2);
	
	parameter N = 8;
	
	output equal, lower, greater;
	input[N-1:0] num1, num2;
	
	assign equal   = (num1 == num2);
	assign lower   = (num1 < num2);
	assign greater = (num1 > num2);

endmodule

module BinaryComparator_16b_RTL(equal, lower, greater, num1, num2);
	
	parameter N = 16;
	
	output equal, lower, greater;
	input[N-1:0] num1, num2;
	
	assign equal   = (num1 == num2);
	assign lower   = (num1 < num2);
	assign greater = (num1 > num2);

endmodule

module BinaryComparator_32b_RTL(equal, lower, greater, num1, num2);
	
	parameter N = 32;
	
	output equal, lower, greater;
	input[N-1:0] num1, num2;
	
	assign equal   = (num1 == num2);
	assign lower   = (num1 < num2);
	assign greater = (num1 > num2);

endmodule

module BinaryComparator_64b_RTL(equal, lower, greater, num1, num2);
	
	parameter N = 64;
	
	output equal, lower, greater;
	input[N-1:0] num1, num2;
	
	assign equal   = (num1 == num2);
	assign lower   = (num1 < num2);
	assign greater = (num1 > num2);

endmodule

module BinaryComparator_4b_SDF(equal, lower, greater, num1, num2);

	parameter N = 4;
	
	output equal, lower, greater;
	input[N-1:0] num1, num2;
	
	MagnitudeComparator_4b_SDF mc0(equal, lower, greater, num1, num2, , ,);
	
endmodule

module BinaryComparator_8b_SDF(equal, lower, greater, num1, num2);
	
	parameter N = 8;
	
	output equal, lower, greater;
	input[N-1:0] num1, num2;
	
	wire[((N/4)*3)-3:0] comp_nets;
	
	MagnitudeComparator_4b_SDF mc0(comp_nets[2], comp_nets[1], comp_nets[0], num1[3:0], num2[3:0], , ,);
	MagnitudeComparator_4b_SDF mc1(equal, lower, greater, num1[7:4], num2[7:4], comp_nets[2], comp_nets[1], comp_nets[0]);
	
endmodule

module BinaryComparator_16b_SDF(equal, lower, greater, num1, num2);
	
	parameter N = 16;
	
	output equal, lower, greater;
	input[N-1:0] num1, num2;
	
	wire[8:0] comp_nets;
	
	MagnitudeComparator_4b_SDF mc0(comp_nets[2], comp_nets[1], comp_nets[0], num1[3:0], num2[3:0], , ,);
	MagnitudeComparator_4b_SDF mc1(comp_nets[5], comp_nets[4], comp_nets[3], num1[7:4], num2[7:4], comp_nets[2], comp_nets[1], comp_nets[0]);
	MagnitudeComparator_4b_SDF mc2(comp_nets[8], comp_nets[7], comp_nets[6], num1[11:8], num2[11:8], comp_nets[5], comp_nets[4], comp_nets[3]);
	MagnitudeComparator_4b_SDF mc3(equal, lower, greater, num1[15:12], num2[15:12], comp_nets[8], comp_nets[7], comp_nets[6]);
	
endmodule

module BinaryComparator_32b_SDF(equal, lower, greater, num1, num2);
	
	parameter N = 32;
	
	output equal, lower, greater;
	input[N-1:0] num1, num2;
	
	wire[((N/4)*3)-4:0] comp_nets;
	
	MagnitudeComparator_4b_SDF mc0(comp_nets[2], comp_nets[1], comp_nets[0], num1[3:0], num2[3:0], , ,);
	MagnitudeComparator_4b_SDF mc1(comp_nets[5], comp_nets[4], comp_nets[3], num1[7:4], num2[7:4], comp_nets[2], comp_nets[1], comp_nets[0]);
	MagnitudeComparator_4b_SDF mc2(comp_nets[8], comp_nets[7], comp_nets[6], num1[11:8], num2[11:8], comp_nets[5], comp_nets[4], comp_nets[3]);
	MagnitudeComparator_4b_SDF mc3(comp_nets[11], comp_nets[10], comp_nets[9], num1[15:12], num2[15:12], comp_nets[8], comp_nets[7], comp_nets[6]);
	MagnitudeComparator_4b_SDF mc4(comp_nets[14], comp_nets[13], comp_nets[12], num1[19:16], num2[19:16], comp_nets[11], comp_nets[10], comp_nets[9]);
	MagnitudeComparator_4b_SDF mc5(comp_nets[17], comp_nets[16], comp_nets[15], num1[23:20], num2[23:20], comp_nets[14], comp_nets[13], comp_nets[12]);
	MagnitudeComparator_4b_SDF mc6(comp_nets[20], comp_nets[19], comp_nets[18], num1[27:24], num2[27:24], comp_nets[17], comp_nets[16], comp_nets[15]);
	MagnitudeComparator_4b_SDF mc7(equal, lower, greater, num1[31:28], num2[31:28], comp_nets[20], comp_nets[19], comp_nets[18]);
	
endmodule


module BinaryComparator_64b_SDF(equal, lower, greater, num1, num2);
	
	parameter N = 64;
	
	output equal, lower, greater;
	input[N-1:0] num1, num2;
	
	wire[((N/4)*3)-4:0] comp_nets;
	
	MagnitudeComparator_4b_SDF mc0(comp_nets[2], comp_nets[1], comp_nets[0], num1[3:0], num2[3:0], , ,);
	MagnitudeComparator_4b_SDF mc1(comp_nets[5], comp_nets[4], comp_nets[3], num1[7:4], num2[7:4], comp_nets[2], comp_nets[1], comp_nets[0]);
	MagnitudeComparator_4b_SDF mc2(comp_nets[8], comp_nets[7], comp_nets[6], num1[11:8], num2[11:8], comp_nets[5], comp_nets[4], comp_nets[3]);
	MagnitudeComparator_4b_SDF mc3(comp_nets[11], comp_nets[10], comp_nets[9], num1[15:12], num2[15:12], comp_nets[8], comp_nets[7], comp_nets[6]);
	MagnitudeComparator_4b_SDF mc4(comp_nets[14], comp_nets[13], comp_nets[12], num1[19:16], num2[19:16], comp_nets[11], comp_nets[10], comp_nets[9]);
	MagnitudeComparator_4b_SDF mc5(comp_nets[17], comp_nets[16], comp_nets[15], num1[23:20], num2[23:20], comp_nets[14], comp_nets[13], comp_nets[12]);
	MagnitudeComparator_4b_SDF mc6(comp_nets[20], comp_nets[19], comp_nets[18], num1[27:24], num2[27:24], comp_nets[17], comp_nets[16], comp_nets[15]);
	MagnitudeComparator_4b_SDF mc7(comp_nets[23], comp_nets[22], comp_nets[21], num1[31:28], num2[31:28], comp_nets[20], comp_nets[19], comp_nets[18]);
	MagnitudeComparator_4b_SDF mc8(comp_nets[26], comp_nets[25], comp_nets[24], num1[35:32], num2[35:32], comp_nets[23], comp_nets[22], comp_nets[21]);
	MagnitudeComparator_4b_SDF mc9(comp_nets[29], comp_nets[28], comp_nets[27], num1[39:36], num2[39:36], comp_nets[26], comp_nets[25], comp_nets[24]);
	MagnitudeComparator_4b_SDF mc10(comp_nets[32], comp_nets[31], comp_nets[30], num1[43:40], num2[43:40], comp_nets[29], comp_nets[28], comp_nets[27]);
	MagnitudeComparator_4b_SDF mc11(comp_nets[35], comp_nets[34], comp_nets[33], num1[47:44], num2[47:44], comp_nets[32], comp_nets[31], comp_nets[30]);
	MagnitudeComparator_4b_SDF mc12(comp_nets[38], comp_nets[37], comp_nets[36], num1[51:48], num2[51:48], comp_nets[35], comp_nets[34], comp_nets[33]);
	MagnitudeComparator_4b_SDF mc13(comp_nets[41], comp_nets[40], comp_nets[39], num1[55:52], num2[55:52], comp_nets[38], comp_nets[37], comp_nets[36]);
	MagnitudeComparator_4b_SDF mc14(comp_nets[44], comp_nets[43], comp_nets[42], num1[59:56], num2[59:56], comp_nets[41], comp_nets[40], comp_nets[39]);
	MagnitudeComparator_4b_SDF mc15(equal, lower, greater, num1[63:60], num2[63:60], comp_nets[44], comp_nets[43], comp_nets[42]);
	
endmodule