module MagnitudeComparator_4b_SDF(o_equal, o_lower, o_greater, num1, num2, i_equal, i_lower, i_greater);
		
	output o_equal, o_lower, o_greater;
	input[3:0] num1, num2;
	inout i_equal, i_lower, i_greater;
	
	pullup(i_equal);
	pulldown(i_lower);
	pulldown(i_greater);
	
	//Creating negative inputs
	wire[3:0] not_nets_num1;
	not(not_nets_num1[0], num1[0]);
	not(not_nets_num1[1], num1[1]);
	not(not_nets_num1[2], num1[2]);
	not(not_nets_num1[3], num1[3]);
	wire[3:0] not_nets_num2;
	not(not_nets_num2[0], num2[0]);
	not(not_nets_num2[1], num2[1]);
	not(not_nets_num2[2], num2[2]);
	not(not_nets_num2[3], num2[3]);
		
	wire[7:0] and_nets_0;
	and(and_nets_0[0], not_nets_num2[0], num1[0]);
	and(and_nets_0[1], not_nets_num1[0], num2[0]);
	and(and_nets_0[2], not_nets_num2[1], num1[1]);
	and(and_nets_0[3], not_nets_num1[1], num2[1]);
	and(and_nets_0[4], not_nets_num2[2], num1[2]);
	and(and_nets_0[5], not_nets_num1[2], num2[2]);
	and(and_nets_0[6], not_nets_num2[3], num1[3]);
	and(and_nets_0[7], not_nets_num1[3], num2[3]);
	
	wire[3:0] nor_nets;
	nor(nor_nets[0], and_nets_0[0], and_nets_0[1]);
	nor(nor_nets[1], and_nets_0[2], and_nets_0[3]);
	nor(nor_nets[2], and_nets_0[4], and_nets_0[5]);
	nor(nor_nets[3], and_nets_0[6], and_nets_0[7]);
	
	wire[5:0] and_nets_1;
	and(and_nets_1[0], and_nets_0[0], nor_nets[1], nor_nets[2], nor_nets[3]);
	and(and_nets_1[1], and_nets_0[1], nor_nets[1], nor_nets[2], nor_nets[3]);
	and(and_nets_1[2], and_nets_0[2], nor_nets[2], nor_nets[3]);
	and(and_nets_1[3], and_nets_0[3], nor_nets[2], nor_nets[3]);
	and(and_nets_1[4], and_nets_0[4], nor_nets[3]);
	and(and_nets_1[5], and_nets_0[5], nor_nets[3]);
	
	//pre-outputs
	wire[2:0] o_nets;
	and(o_nets[0], nor_nets[0], nor_nets[1], nor_nets[2], nor_nets[3]);
	or(o_nets[1], and_nets_1[0], and_nets_1[2], and_nets_1[4], and_nets_0[6]);
	or(o_nets[2], and_nets_1[1], and_nets_1[3], and_nets_1[5], and_nets_0[7]);
	
	//outputs
	wire[1:0] and_nets_2;
	and(o_equal, o_nets[0], i_equal);
	and(and_nets_2[0], i_greater, o_nets[0]);
	or(o_greater, and_nets_2[0], o_nets[1]);
	and(and_nets_2[1], i_lower, o_nets[0]);
	or(o_lower, and_nets_2[1], o_nets[2]);
	
	
endmodule