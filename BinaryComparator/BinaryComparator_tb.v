`include "BinaryComparator.v"

module main_tb();

	wire carry_out;
	wire equal, lower, greater;
	
	BinaryComparator_64b_SDF u0(equal, lower, greater, 64'd1200, 64'd1200);
	
	initial begin
		$dumpfile("BinaryComparator_test.vcd");
		$dumpvars(0, main_tb);
		
		
		#5 $finish;
	end

endmodule