`include "FullAdder.v"

module main_tb();

	integer i;
	wire carry_in, a, b;
	wire carry_out, sum;
	reg[2:0] counter;
	
	assign carry_in = counter[0];
	assign b = counter[1];
	assign a = counter[2];
	
	//FullAdder_RTL u0(sum, carry_out, carry_in, a, b);
	//FullAdder_SDF u0(sum, carry_out, carry_in, a, b);
	FullAdder_TTab u0(sum, carry_out, carry_in, a, b);
	initial begin
		$dumpfile("FullAdder_test.vcd");
		$dumpvars(0, main_tb);
		
		for(i = 0; i < 8; i = i + 1) begin
			#5 counter = i;
		end
		#5 $finish;
	end

endmodule